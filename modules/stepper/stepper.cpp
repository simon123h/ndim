#include "stepper.hpp"
#include <cmath>

Stepper::Stepper(double dt) : dt(dt) {}

Stepper::~Stepper() {}

// set the integration timestep
void Stepper::setTimestep(double dt) { this->dt = dt; }
// set the model to be integrated
void Stepper::setModel(Model *model) { this->model = model; }

// make multiple timesteps
void Stepper::steps(int steps) {
  for (int i = 0; i < steps; i++)
    step();
}

// integrate for a period of time
void Stepper::integrate(double T) { steps(std::round(T / dt)); }
