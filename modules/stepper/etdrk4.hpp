#pragma once
#include "math/math.hpp"
#include "model/ode.hpp"
#include "stepper.hpp"

/*
 * An Exponential-Time-Differencing formulation of the famous Runge-Kutta-4
 * scheme, according to Matthews & Cox (2002) "Exponential Time Differencing for
 * stiff PDEs".
 * Its use is yet restricted to ODEs, PDEs and child classes.
 */

class ETDRK4 : public Stepper {
private:
  // the constants of the integration scheme
  std::vector<double> const0, const1, const2, const3, const4;
  // temporary data vectors, allocated only once
  std::vector<double> a, b, c;

public:
  ETDRK4(double dt = 0.1);
  ~ETDRK4();

  // pointer to ODE class model
  // model must be of type ODE or child
  ODE *ode = nullptr;

  // set the integration timestep
  virtual void setTimestep(double dt);
  // set the model to be integrated, invokes setTimestep
  // TODO: throw error when model is not an ODE*
  // or assure likewise, that it is
  virtual void setModel(Model *model);
  // perform a timestep
  virtual void step();
};