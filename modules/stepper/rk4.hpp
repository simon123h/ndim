#pragma once
#include "stepper.hpp"
#include <vector>

class RK4 : public Stepper {
private:
public:
  RK4(double dt = 0.1);
  ~RK4();
  // make a single RK4 timestep
  void step();
  // the Butcher's tableau of the scheme
  std::vector<double> butchers{1. / 6., 1. / 3., 1. / 3., 1. / 6.};
};

RK4::RK4(double dt) : Stepper(dt) {}

RK4::~RK4() {}

void RK4::step() {
  // generate constants
  double t = model->t;
  std::vector<double> temp(model->dimension);
  std::vector<double> k1 = model->rhs(model->vars, t);
  for (int i = 0; i < model->dimension; i++)
    temp[i] = model->vars[i] + 0.5 * dt * k1[i];
  std::vector<double> k2 = model->rhs(temp, t + dt / 2.);
  for (int i = 0; i < model->dimension; i++)
    temp[i] = model->vars[i] + 0.5 * dt * k2[i];
  std::vector<double> k3 = model->rhs(temp, t + dt / 2.);
  for (int i = 0; i < model->dimension; i++)
    temp[i] = model->vars[i] + dt * k3[i];
  std::vector<double> k4 = model->rhs(temp, t + dt);
  // sum up
  for (int i = 0; i < model->dimension; i++)
    model->vars[i] += dt * butchers[0] * k1[i] + dt * butchers[1] * k2[i] +
                      dt * butchers[2] * k3[i] + dt * butchers[3] * k4[i];
  model->t += dt;
}