#pragma once
#include "stepper.hpp"
#include <vector>

class Euler : public Stepper {
private:
public:
  Euler(int dt = 0.1);
  ~Euler();
  // make a single Euler timestep
  void step();
};

Euler::Euler(int dt) : Stepper(dt) {}

Euler::~Euler() {}

void Euler::step() {
  std::vector<double> rhs = model->rhs(model->vars, model->t);
  // std::vector<double> vars = model->getVars();
  for (int i = 0; i < model->dimension; i++)
    model->vars[i] += rhs[i] * dt;
  // model->setVars(vars);
  model->t += dt;
}