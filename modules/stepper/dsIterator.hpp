#if !defined(__dsiterator__hpp)
#define __dsiterator__hpp
#include "stepper/stepper.hpp"

class DSIterator : public Stepper {
private:
public:
  DSIterator();
  ~DSIterator();
  void step();
};

DSIterator::DSIterator() : Stepper(1) {}

DSIterator::~DSIterator() {}

// simple timestepping for dynamical systems
void DSIterator::step() {
  std::vector<double> rhs = model->rhs(model->vars);
  for (int i = 0; i < model->dimension; i++)
    model->vars[i] += rhs[i];
}

#endif