#include "etdrk4.hpp"
#include <iostream>

ETDRK4::ETDRK4(double dt) : Stepper(dt) {}

ETDRK4::~ETDRK4() {}

void ETDRK4::setTimestep(double dt) {
  this->dt = dt;
  // return if ode undefined
  if (ode == nullptr)
    return;
  // evaluate linear part of the ODE
  std::vector<double> linear =
      ode->linear(std::vector<double>(ode->dimension, 1.));
  int steps = 64;
  std::complex<long double> x;
  std::complex<long double> longFactors[4];
  for (int i = 0; i < ode->dimension; i++) {
    // const0 can be calculated right away
    const0[i] = exp(dt * linear[i] / 2.);
    // calculate others with contour integral method
    for (int j = 0; j < 4; j++)
      longFactors[j] = 0;
    for (int k = 0; k < steps; k++) {
      // let x rotate in complex space around critical value
      // sufficient to rotate only half circle and then take real part later
      x = std::complex<long double>(dt * linear[i]) +
          exp(std::complex<long double>(math::I * math::PI *
                                        ((k + 0.5) / steps)));
      // constant formulas
      longFactors[0] += (exp(x / 2.l) - 1.l) / x;
      longFactors[1] +=
          (-4.l - x + exp(x) * (4.l + (-3.l + x) * x)) / x / x / x;
      longFactors[2] += (2.l + x + exp(x) * (-2.l + x)) / x / x / x;
      longFactors[3] +=
          (-4.l + (-3.l - x) * x + exp(x) * (4.l - x)) / x / x / x;
    }
    // average and take real part only, as all constants are real
    const1[i] = dt * longFactors[0].real() / ((double)steps);
    const2[i] = dt * longFactors[1].real() / ((double)steps);
    const3[i] = dt * longFactors[2].real() / ((double)steps);
    const4[i] = dt * longFactors[3].real() / ((double)steps);
  }
}

// set the model to be integrated
void ETDRK4::setModel(Model *model) {
  this->model = model;
  this->ode = dynamic_cast<ODE *>(model);
  // update dimension of scheme vectors
  const0 = std::vector<double>(ode->dimension);
  const1 = std::vector<double>(ode->dimension);
  const2 = std::vector<double>(ode->dimension);
  const3 = std::vector<double>(ode->dimension);
  const4 = std::vector<double>(ode->dimension);
  a = std::vector<double>(ode->dimension);
  b = std::vector<double>(ode->dimension);
  c = std::vector<double>(ode->dimension);
  // invoke setTimestep, as the linearity might have changed
  setTimestep(dt);
}

void ETDRK4::step() {
  std::vector<double> vars = ode->getVars();
  double t = ode->t;
  // calculate parts a, b, c of the ETDRK4 scheme
  std::vector<double> temp1 = ode->nonlinear(vars, t);
  for (int i = 0; i < ode->dimension; i++)
    a[i] = const0[i] * vars[i] + const1[i] * temp1[i];
  std::vector<double> temp2 = ode->nonlinear(a, t + dt / 2.);
  for (int i = 0; i < ode->dimension; i++)
    b[i] = const0[i] * vars[i] + const1[i] * temp2[i];
  std::vector<double> temp3 = ode->nonlinear(b, t + dt / 2.);
  for (int i = 0; i < ode->dimension; i++)
    c[i] = const0[i] * a[i] - const1[i] * temp1[i] + 2. * const1[i] * temp3[i];
  std::vector<double> temp4 = ode->nonlinear(c, t + dt);
  // add them together
  for (int i = 0; i < ode->dimension; i++)
    vars[i] = vars[i] * const0[i] * const0[i] + const2[i] * temp1[i] +
              2. * const3[i] * (temp2[i] + temp3[i]) + const4[i] * temp4[i];
  ode->setVars(vars);
  ode->t += dt;
}