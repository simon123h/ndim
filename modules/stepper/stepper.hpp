#pragma once
#include "model/model.hpp"
#include <iostream>

class Stepper {
private:
public:
  Stepper(double dt);
  virtual ~Stepper();

  // the model of interest
  Model *model = nullptr;

  // the time step size
  double dt;

  // getter/setter
  virtual void setTimestep(double dt);
  virtual void setModel(Model *model);

  // make a single timestep
  virtual void step() = 0;
  // make multiple timesteps
  virtual void steps(int steps);
  // integrate for a period of time
  virtual void integrate(double T);
};