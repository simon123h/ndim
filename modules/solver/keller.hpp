#pragma once
#include "math/math.hpp"
#include "solver.hpp"

class PseudoarclengthContinuation : public Solver {
private:
  Eigen::MatrixXd generate_extended_jacobian(std::vector<double> x, double t);

public:
  PseudoarclengthContinuation();
  ~PseudoarclengthContinuation();

  // pointer to the principal continuation parameter of the model
  double *parameter;
  // the step size
  double stepSize = 1e-3;
  // convergence criterion: convergent if |dX|^2 < epsilon
  double epsilon = 1e-10;
  // maximum number of newton steps until abort
  int maxSteps = 99;

  // parameters for adaptive step size
  bool adaptiveStepsize = true;
  double ds_max = 5e-2;
  double ds_min = 1e-6;
  // desired number of newton iterations for adaptive stepsize control
  int desired_newton_steps = 2;
  // 'penalty' factors when more/less than desired_newton_steps are performed
  double ds_decrease_factor = 0.2;
  double ds_increase_factor = 1.05;
  // rescale the parameter constraint, for numerical stability
  // may be decreased for, e.g., very sharp folds
  double constraint_scale = 1;

  bool solve();
};