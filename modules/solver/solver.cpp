#include "solver.hpp"
#include "generic.hpp"
#include <Eigen/Dense>

Solver::Solver() {}

Solver::~Solver() {}

// set the model to be integrated
void Solver::setModel(Model *model) { this->model = model; }

void Solver::detectBifurcations() {
  double new_det_sign = math::det_sign(jac);

  double det_near_zero = false;
  if (model->is_time_discrete) {
    calculateEigenvalues();
    double det = 1;
    for (unsigned i = 0; i < eigenvalues.size(); i++)
      det *= eigenvalues[i].real();
    new_det_sign = det > 0 ? 1 : -1;
    if (std::abs(det) < 1e-3)
      det_near_zero = true;
  }
  bool det_sign_change = ((new_det_sign * jac_det_sign) < 0);
  jac_det_sign = new_det_sign;
  // if it did: we passed a bifurcation point
  if (det_sign_change || det_near_zero)
    point_type = "BP";
  else
    point_type = "RG";
}

void Solver::calculateEigenvalues() {
  // TODO: prevent recalculation of eigenvalues, if already calculated
  eigenvalues = math::eigenvalues(jac);
  // eigenvalues of discrete dynamical systems need to be converted
  if (model->is_time_discrete)
    for (unsigned i = 0; i < eigenvalues.size(); i++)
      eigenvalues[i] = std::log(eigenvalues[i] + 1.);
}

// check stability of the current solution via Jacobian
// returns true if stable
void Solver::checkStability() {
  // calculate the eigenvalues of the Jacobian
  calculateEigenvalues();

  // Count number of eigenvalues with positive real part
  unsigned n_pos_eigenvalues = 0;
  for (unsigned i = 0; i < eigenvalues.size(); i++)
    if (eigenvalues[i].real() > 0)
      n_pos_eigenvalues++;
  // stable, if all eigenvalues are negative
  std::cout << n_pos_eigenvalues << std::endl;
  is_point_stable = n_pos_eigenvalues == 0;
}