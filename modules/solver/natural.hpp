#pragma once
#include "math/math.hpp"
#include "solver.hpp"

class NaturalContinuation : public Solver {
private:
public:
  NaturalContinuation();
  ~NaturalContinuation();

  double epsilon = 1e-8;
  int maxSteps = 999;
  double stepSize = 1e-3;
  double *parameter;

  bool solve();
};