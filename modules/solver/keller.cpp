#include "keller.hpp"
#include "generic.hpp"
#include <algorithm>

PseudoarclengthContinuation::PseudoarclengthContinuation() : Solver() {}

PseudoarclengthContinuation::~PseudoarclengthContinuation() {}

Eigen::MatrixXd
PseudoarclengthContinuation::generate_extended_jacobian(std::vector<double> x,
                                                        double t) {
  int N = model->dimension;
  std::vector<double> rhs = model->rhs(x, model->t);
  std::vector<std::vector<double>> rhsJac = model->jacobian(x, model->t);
  Eigen::MatrixXd jacobian = Eigen::MatrixXd::Zero(N + 1, N + 1);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      jacobian(i, j) = rhsJac[i][j];
  // last column of extended jacobian: d(rhs)/d(parameter)
  double h = 1e-8;
  *parameter += h;
  std::vector<double> deviated = model->rhs(x, t);
  *parameter -= h;
  for (int i = 0; i < N; i++)
    jacobian(i, N) = (deviated[i] - rhs[i]) / h;
  return jacobian;
}

bool PseudoarclengthContinuation::solve() {
  double oldparam = *parameter;
  std::vector<double> x_old = model->getVars();
  int count = 0;
  int N = model->dimension;

  // TODO: use tangent vector from previous run?
  // build extended jacobian in (vars, parameter)-space for tangent calculation
  std::vector<double> rhs = model->rhs(x_old, model->t);
  std::vector<std::vector<double>> rhsJac = model->jacobian(x_old, model->t);
  jac = Eigen::MatrixXd::Zero(N + 1, N + 1);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      jac(i, j) = rhsJac[i][j];
  // last column of extended jacobian: d(rhs)/d(parameter)
  double h = 1e-8;
  *parameter += h;
  std::vector<double> deviated = model->rhs(x_old, model->t);
  *parameter -= h;
  for (int i = 0; i < N; i++)
    jac(i, N) = (deviated[i] - rhs[i]) / h;

  // compute tangent by solving (jac)*tangent=0
  Eigen::VectorXd zero = Eigen::VectorXd::Zero(N + 1);
  zero[N] = 1;   // for solvability
  jac(N, N) = 1; // for solvability
  // solve with QR decomposition
  Eigen::VectorXd tangent = jac.colPivHouseholderQr().solve(zero);
  // normalize
  double tnorm = tangent.norm();
  for (unsigned i = 0; i < tangent.size(); i++)
    tangent[i] /= tnorm;

  // make initial guess: X -> X + dS * tangent
  std::vector<double> x_new = x_old;
  for (int i = 0; i < N; i++)
    x_new[i] += stepSize * tangent[i];
  double newparam = *parameter + stepSize * tangent[N];

  // Newton iterations to find new solution X
  bool converged = false;
  while (!converged && count < maxSteps) {
    *parameter = newparam;
    rhs = model->rhs(x_new, model->t);
    rhsJac = model->jacobian(x_new, model->t);

    // build extended jacobian in (vars, parameter)-space
    for (int i = 0; i < N; i++)
      for (int j = 0; j < N; j++)
        jac(i, j) = rhsJac[i][j];

    // TODO: implement branch switching here

    // last column of extended jacobian: d(rhs)/d(parameter)
    double h = 1e-8;
    *parameter += h;
    std::vector<double> deviated = model->rhs(x_new, model->t);
    *parameter -= h;
    for (int i = 0; i < N; i++)
      jac(i, N) = (deviated[i] - rhs[i]) / h;

    // last row of extended jacobian: tangent vector
    for (int j = 0; j < N + 1; j++)
      jac(N, j) = tangent[j];

    // extended rhs: model's rhs + arclength condition
    Eigen::VectorXd erhs = Eigen::VectorXd::Zero(N + 1);
    for (int i = 0; i < N; i++) {
      erhs[i] = rhs[i];
      erhs[N] += (x_new[i] - x_old[i]) * tangent[i];
    }
    erhs[N] += (newparam - oldparam) * tangent[N] * constraint_scale - stepSize;

    // solve linear system to retrieve step in solution space
    Eigen::VectorXd dX = jac.colPivHouseholderQr().solve(erhs);

    // make step in solution space: vars = vars - dX
    for (unsigned i = 0; i < x_new.size(); i++)
      x_new[i] -= dX[i];
    newparam -= dX[N];

    // measure convergence and counts
    count++;
    converged = dX.norm() < epsilon;
  }

  // if converged: update models variables
  if (converged) {
    model->setVars(x_new);

    // adjust sign of stepSize according to current continuation direction
    // this is crucial for continuation of folds.
    if (newparam - oldparam < 0)
      stepSize = -std::fabs(stepSize);
    else
      stepSize = std::abs(stepSize);

    *parameter = newparam;

    // post-processing shall happen with rhsJac only, not extended Jacobian
    jac = math::eigenMat(rhsJac);

  } else {
    model->setVars(x_old);
    *parameter = oldparam;
  }

  // adaptive stepsize
  if (adaptiveStepsize) {
    if (count > desired_newton_steps) {
      if (std::abs(stepSize) <= ds_min)
        return false;
      int sign = stepSize < 0 ? -1 : 1;
      stepSize =
          std::max(std::abs(stepSize) * ds_decrease_factor, ds_min) * sign;
      // redo continuation
      model->setVars(x_old);
      *parameter = oldparam;
      return solve();
    }
    if (count < desired_newton_steps) {
      int sign = stepSize < 0 ? -1 : 1;
      stepSize =
          std::min(std::abs(stepSize) * ds_increase_factor, ds_max) * sign;
    }
  }

  return converged;
}