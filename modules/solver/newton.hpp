#pragma once
#include "math/math.hpp"
#include "solver.hpp"

class Newton : public Solver {
private:
public:
  Newton();
  ~Newton();

  double epsilon = 1e-8;
  int maxSteps = 999;

  bool solve();
};