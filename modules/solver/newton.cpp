#include "newton.hpp"
#include "generic.hpp"
#include <Eigen/Dense>

Newton::Newton() : Solver() {}

Newton::~Newton() {}

bool Newton::solve() {
  std::vector<double> vars = model->getVars();
  int count = 0;
  bool converged = false;
  while (!converged && count < maxSteps) {
    Eigen::VectorXd rhs = math::eigenVec(model->rhs(vars, model->t));
    jac = math::eigenMat(model->jacobian(vars, model->t));
    // solve linear system: (jac)*dX = (rhs) with QR decomposition
    Eigen::VectorXd dX = jac.colPivHouseholderQr().solve(rhs);
    // make step in solution space: vars = vars - dX
    for (unsigned i = 0; i < vars.size(); i++)
      vars[i] -= dX[i];
    // measure convergence and counts
    count++;
    converged = dX.norm() < epsilon;
  }
  model->setVars(vars);
  return converged;
}
