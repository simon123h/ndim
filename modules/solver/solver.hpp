#pragma once
#include "math/math.hpp"
#include "model/model.hpp"

class Solver {
public:
  Solver();
  virtual ~Solver();

  // the model of interest
  Model *model = nullptr;

  // solve the model, returns true if successful
  virtual bool solve() = 0;

  // getter/setter
  virtual void setModel(Model *model);

  // bifurcation detection
  virtual void detectBifurcations();

  // calculate eigenvalues of the Jacobian
  virtual void calculateEigenvalues();

  // check stability of the current solution via Jacobian
  virtual void checkStability();

  // the current vector of eigenvalues
  std::vector<complex> eigenvalues;
  // is the current point a stable solution?
  bool is_point_stable = false;
  // the type of the current point
  std::string point_type = "RG";
  // the current rank of the jacobian
  int rank = -1;

protected:
  // the Jacobian of the model at the current solution point
  Eigen::MatrixXd jac;
  // sign of the determinant of the jacobian
  double jac_det_sign;
};