#pragma once

#include <Eigen/Dense>
#include <cmath>
#include <complex>
#include <vector>
typedef std::complex<double> complex;

namespace math {

// constants
const double PI(
    3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679);
const complex I(0.0, 1.0);

// like pythons numpy.linspace
std::vector<double> linspace(double from, double to, int num);
// like pythons numpy.arange
std::vector<double> range(double from, double to, double step);
// like pythons numpy.logspace
std::vector<double> logspace(double from, double to, int num, double base = 10);

// calculate binomial coefficients
int binomialCoeff(int n, int k);

// convert STL vector to Eigen::Vector
Eigen::VectorXd eigenVec(const std::vector<double> &A);
// convert STL matrix to Eigen::Matrix
Eigen::MatrixXd eigenMat(const std::vector<std::vector<double>> &A);

// solve a linear system Ax=b
std::vector<double> linear_solve(const std::vector<std::vector<double>> &A,
                                 const std::vector<double> &b);

// determinant of a matrix
double det(const std::vector<std::vector<double>> &A);
// logarithm of absolute value of a matrix via QR decomposition
double logAbsDet(const std::vector<std::vector<double>> &A);
// sign of the determinant of a matrix via LU decomposition
double det_sign(const std::vector<std::vector<double>> &A);
// sign of the determinant of a matrix via LU decomposition (Eigen version)
double det_sign(const Eigen::MatrixXd &A);

// calculate eigenvalues of a matrix
std::vector<complex> eigenvalues(const std::vector<std::vector<double>> &A);
// calculate eigenvalues of a matrix (Eigen version)
std::vector<complex> eigenvalues(const Eigen::MatrixXd &A);

} // namespace math