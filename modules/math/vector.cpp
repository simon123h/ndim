#include "vector.hpp"

// element wise absolute of vector
template <typename T> std::vector<double> math::abs(const std::vector<T> &x) {
  std::vector<double> result(x.size());
  for (unsigned i = 0; i < x.size(); i++)
    result[i] = std::abs(x[i]);
  return result;
}
template std::vector<double> math::abs<double>(const std::vector<double> &x);
template std::vector<double> math::abs<complex>(const std::vector<complex> &x);

// element wise real part of vector
std::vector<double> math::real(const std::vector<complex> &x) {
  std::vector<double> result(x.size());
  for (unsigned i = 0; i < x.size(); i++)
    result[i] = x[i].real();
  return result;
}
// element wise imaginary part of vector
std::vector<double> math::imag(const std::vector<complex> &x) {
  std::vector<double> result(x.size());
  for (unsigned i = 0; i < x.size(); i++)
    result[i] = x[i].imag();
  return result;
}

// element sum of a vector
double math::sum(const std::vector<double> &x) {
  double result = 0;
  for (unsigned i = 0; i < x.size(); i++)
    result += x[i];
  return result;
}
// element product of a vector
double math::prod(const std::vector<double> &x) {
  double result = 1;
  for (unsigned i = 0; i < x.size(); i++)
    result *= x[i];
  return result;
}
// norm of a vector
double math::norm(const std::vector<double> &x) {
  double result = 0;
  for (unsigned i = 0; i < x.size(); i++)
    result += x[i] * x[i];
  return sqrt(result);
}

// reshape vector into matrix
std::vector<std::vector<double>> math::matrix(const std::vector<double> &x,
                                              int Nx, int Ny) {
  std::vector<std::vector<double>> result(Nx, std::vector<double>(Ny));
  for (int i = 0; i < Nx; i++)
    for (int j = 0; j < Ny; j++)
      result[i][j] = x[i * Ny + j];
  return result;
}

// transpose a matrix
std::vector<std::vector<double>>
math::transpose(const std::vector<std::vector<double>> &x) {
  int Nx = x.size();
  int Ny = x[0].size();
  std::vector<std::vector<double>> result(Ny, std::vector<double>(Nx));
  for (int i = 0; i < Nx; i++)
    for (int j = 0; j < Ny; j++)
      result[j][i] = x[i][j];
  return result;
}