#include "fft.hpp"
#include <algorithm>

/*
 * Wrapper class for n-dimensional real-to-complex fast Fourier transforms
 * and related methods.
 */

// initialize FFT with vector of gridpoints
FFT::FFT(const std::vector<int> &points) : points(points) {
  rank = points.size();
  realPoints = 1;
  fourierPoints = 1;
  for (int i = 0; i < rank; i++)
    realPoints *= points[i];
  for (int i = 0; i < rank - 1; i++)
    fourierPoints *= points[i];
  fourierPoints *= points[rank - 1] / 2 + 1;
  // memory alloc alignment, cf.
  // http://www.fftw.org/fftw3_doc/SIMD-alignment-and-fftw_005fmalloc.html
  rtemp = fftw_alloc_real(realPoints);
  ctemp = reinterpret_cast<complex *>(fftw_alloc_complex(fourierPoints));
  // plans, cast std::complex to fftw_complex, cf.
  // http://www.fftw.org/fftw3_doc/Complex-numbers.html
  plan_fft = fftw_plan_dft_r2c(rank, points.data(), rtemp,
                               reinterpret_cast<fftw_complex *>(ctemp),
                               FFTW_MEASURE | FFTW_DESTROY_INPUT);
  plan_ifft = fftw_plan_dft_c2r(rank, points.data(),
                                reinterpret_cast<fftw_complex *>(ctemp), rtemp,
                                FFTW_MEASURE | FFTW_DESTROY_INPUT);
}

// initialize 1d FFT with number of gridpoints
FFT::FFT(int points) : FFT(1, points) {}
// initialize <rank>-dim. FFT with <points> gridpoints in each dimension
FFT::FFT(int rank, int points) : FFT(std::vector<int>(rank, points)) {}

// FFT destructor
FFT::~FFT() {
  fftw_free(rtemp);
  fftw_free(ctemp);
  fftw_destroy_plan(plan_fft);
  fftw_destroy_plan(plan_ifft);
}

// real-to-complex Fourier transform
void FFT::fft(double *__restrict__ in, complex *__restrict__ out) {
  // normalize and copy to rtemp
  double norm = 1. / (double)realPoints;
  for (int i = 0; i < realPoints; i++)
    rtemp[i] = in[i] * norm;
  // transform
  fftw_execute(plan_fft);
  // copy to out
  for (int i = 0; i < fourierPoints; i++)
    out[i] = ctemp[i];
}

// real-to-complex inverse Fourier transform
void FFT::ifft(complex *__restrict__ in, double *__restrict__ out) {
  // copy to ctemp
  for (int i = 0; i < fourierPoints; i++)
    ctemp[i] = in[i];
  // transform
  fftw_execute(plan_ifft);
  // copy to out
  for (int i = 0; i < realPoints; i++)
    out[i] = rtemp[i];
}

// real-to-complex Fourier transform
std::vector<complex> FFT::fft(std::vector<double> &in) {
  std::vector<complex> result(fourierPoints);
  fft(in.data(), result.data());
  return result;
}
// real-to-complex inverse Fourier transform
std::vector<double> FFT::ifft(std::vector<complex> &in) {
  std::vector<double> result(realPoints);
  ifft(in.data(), result.data());
  return result;
}

// generate the k-vectors for the fourier space
std::vector<std::vector<double>> FFT::fftfreq(const std::vector<double> &Ls) {
  std::vector<std::vector<double>> k(rank, std::vector<double>(fourierPoints));
  int divisor = fourierPoints;
  for (int d = 0; d < rank; d++) {
    int nmod = (d != rank - 1) ? points[d] : points[d] / 2 + 1;
    divisor /= nmod;
    for (int i = 0; i < fourierPoints; i++) {
      k[d][i] = (i / divisor) % nmod;
      if (k[d][i] > points[d] / 2)
        k[d][i] -= points[d];
      k[d][i] *= 2 * math::PI / Ls[d];
    }
  }
  return k;
}

// generate the k-vector for the fourier space
std::vector<double> FFT::fftfreq(double L) {
  return fftfreq(std::vector<double>(rank, L))[0];
}

// generate the squared k-vector for the fourier space
std::vector<double> FFT::fftfreqsq(const std::vector<double> &Ls) {
  std::vector<std::vector<double>> k = fftfreq(Ls);
  std::vector<double> ksq(fourierPoints);
  for (int i = 0; i < rank; i++)
    for (unsigned j = 0; j < k[i].size(); j++)
      ksq[j] += k[i][j] * k[i][j];
  return ksq;
}

// generate the indices of the fourier coefficients,
std::vector<std::vector<int>> FFT::indices() {
  // generate k-vector with all lengths set to 2*pi
  std::vector<std::vector<double>> k =
      fftfreq(std::vector<double>(rank, 2 * math::PI));
  // convert to integer return
  std::vector<std::vector<int>> coeff(fourierPoints, std::vector<int>(rank));
  for (int d = 0; d < rank; d++)
    for (int i = 0; i < fourierPoints; i++)
      coeff[i][d] = (int)k[d][i];
  return coeff;
}

// convert from abolute index of mode to indices of fourier coefficients
std::vector<int> FFT::index2pos(int n) {
  std::vector<int> pos(rank);
  int divisor = fourierPoints;
  for (int d = 0; d < rank; d++) {
    int nmod = (d != rank - 1) ? points[d] : points[d] / 2 + 1;
    divisor /= nmod;
    pos[d] = (n / divisor) % nmod;
    if (pos[d] > points[d] / 2)
      pos[d] -= points[d];
  }
  return pos;
}

// convert from indices of fourier coefficients to abolute index of mode
int FFT::pos2index(std::vector<int> pos) {
  int n = 0;
  int mul = fourierPoints;
  for (int d = 0; d < rank; d++) {
    int nmod = (d != rank - 1) ? points[d] : points[d] / 2 + 1;
    mul /= nmod;
    if (pos[d] < 0)
      pos[d] += points[d];
    n += pos[d] * mul;
  }
  return n;
}

// find a fast discretization points number higher than int number
int FFT::getGoodFFTWNumber(int number) {
  // find next power of 2
  int j = 1;
  for (; j < number; j *= 2)
    ;
  if (j < 8)
    return j;
  else {
    // divide by 8 and find next multiple of resulting number
    j /= 8;
    int l = 4;
    for (; l * j < number; l++)
      ;
    return l * j;
  }
}

// interpolate a n-dim. real vector by padding in fourier space
std::vector<double> FFT::interpolate(const std::vector<double> &x,
                                     std::vector<int> oldNs,
                                     std::vector<int> newNs) {

  int rank = oldNs.size();
  bool identical = true;
  for (int i = 0; i < rank; i++)
    identical = identical && oldNs[i] == newNs[i];
  if (identical)
    return x;
  FFT *fft1 = new FFT(oldNs);
  FFT *fft2 = new FFT(newNs);
  std::vector<double> oldX = x;
  std::vector<complex> oldF = fft1->fft(oldX);
  std::vector<complex> newF(fft2->fourierPoints);
  // vector of old position indices
  std::vector<std::vector<int>> pos = fft1->indices();
  // for each position in old vector
  for (int i = 0; i < fft1->fourierPoints; i++) {
    // generate position in new vector
    int i2 = fft2->pos2index(pos[i]);
    // write old data to new data
    newF[i2] = oldF[i] * (double)fft2->realPoints / (double)fft2->realPoints;
  }
  std::vector<double> newX = fft2->ifft(newF);
  delete fft1;
  delete fft2;
  return newX;
}
