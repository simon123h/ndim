#pragma once

#include "math.hpp"
#include <fftw3.h>
#include <iostream>
#include <vector>

// real-to-complex n-dimensional FFT
class FFT {
private:
  // FFTW plans
  fftw_plan plan_fft, plan_ifft;
  // global temporary arrays for calculations
  complex *ctemp;
  double *rtemp;

public:
  // initialize FFT with vector of gridpoints
  FFT(const std::vector<int> &points);
  // initialize 1d FFT with number of gridpoints
  FFT(int points);
  // initialize <rank>-dim. FFT with <points> gridpoints in each dimension
  FFT(int rank, int points);
  // destructor
  ~FFT();
  // rank of the FFT transform (1d, 2d, ..., nd)
  int rank;
  // vector of grid points
  std::vector<int> points;
  // total number of real space / fourier space grid points
  int realPoints;
  int fourierPoints;
  // plain array transformations
  void fft(double *in, complex *out);
  void ifft(complex *in, double *out);
  // vector equivalents
  std::vector<complex> fft(std::vector<double> &in);
  std::vector<double> ifft(std::vector<complex> &in);
  // generate the k-vector for the fourier space
  std::vector<std::vector<double>> fftfreq(const std::vector<double> &Ls);
  std::vector<double> fftfreq(double L);
  // generate the squared k-vector for the fourier space
  std::vector<double> fftfreqsq(const std::vector<double> &Ls);
  // generate the indices of the fourier coefficients
  std::vector<std::vector<int>> indices();
  // convert from abolute index of mode to indices of fourier coefficients
  std::vector<int> index2pos(int n);
  // convert from indices of fourier coefficients to abolute index of mode
  int pos2index(std::vector<int> pos);
  // find a fast discretization points number higher than int number
  static int getGoodFFTWNumber(int number);
  // interpolate a n-dim. real vector by padding in fourier space
  static std::vector<double> interpolate(const std::vector<double> &x,
                                         std::vector<int> oldNs,
                                         std::vector<int> newNs);
};