#pragma once
#include "math.hpp"
#include <vector>

class Mesh {
private:
public:
  Mesh(const std::vector<double> &size, const std::vector<int> &points);
  virtual ~Mesh();
  // the dimension of the mesh (1d, 2d, ..., nd)
  int rank;
  // vector of the number of points per dimension [Nx, Ny, ...]
  std::vector<int> points;
  // the total number of gridpoints: Nx * Ny * ...
  int npoints;
  // vector of the dimension sizes [Lx, Ly, ...]
  std::vector<double> size;
  // the total area of the mesh Lx * Ly * ...
  double area;
  // list of the spatial coordinates of the discrete points
  std::vector<std::vector<double>> x;

  // generate the indices (i, j, k, ...) for each gridpoint
  std::vector<std::vector<int>> indices();
  // convert 1d-index-number into n-dim. position index tuple (i, j, k, ...)
  std::vector<int> index2pos(int n);
  // convert n-dim. position index tuple (i, j, k, ...) into 1d-index-number
  int pos2index(const std::vector<int> &pos);
};
