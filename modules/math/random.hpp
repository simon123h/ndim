#pragma once
#include "math.hpp"
/*
 * A module providing some functions concerning random numbers
 */
namespace math {
extern int randomSeed;
double random();
template <typename T> T random();
double random(double from, double to);
double randInt(int from, int to);
std::vector<double> randoms(int N, double amp = 1.);
template <typename T> std::vector<T> randoms(int N, double amp = 1.);
} // namespace math
