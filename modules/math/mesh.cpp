#include "mesh.hpp"
#include "math.hpp"

Mesh::Mesh(const std::vector<double> &size, const std::vector<int> &points)
    : points(points), size(size) {
  // get rank from points vector
  rank = points.size();
  // make sure points and size vectors have the same size
  this->size.resize(rank);
  // count total number of points and total area
  npoints = 1;
  area = 1;
  for (unsigned i = 0; i < points.size(); i++) {
    npoints *= points[i];
    area *= size[i];
  }

  // generate mesh points' coordinates
  x = std::vector<std::vector<double>>(rank, std::vector<double>(npoints));
  int divisor = npoints;
  for (int d = 0; d < rank; d++) {
    int nmod = points[d];
    divisor /= nmod;
    for (int i = 0; i < npoints; i++)
      x[d][i] = ((i / divisor) % nmod) * size[d] / nmod;
  }
}

Mesh::~Mesh() {}

// generate the indices (i, j, k, ...) for each gridpoint
std::vector<std::vector<int>> Mesh::indices() {
  std::vector<std::vector<int>> coeff =
      std::vector<std::vector<int>>(npoints, std::vector<int>(rank));
  int divisor = npoints;
  for (int d = 0; d < rank; d++) {
    int nmod = points[d];
    divisor /= nmod;
    for (int i = 0; i < npoints; i++)
      coeff[i][d] = ((i / divisor) % nmod);
  }
  return coeff;
}

// convert 1d-index-number into n-dim. coordinate index tuple (i, j, k)
std::vector<int> Mesh::index2pos(int n) {
  std::vector<int> pos(rank);
  int divisor = npoints;
  for (int d = 0; d < rank; d++) {
    divisor /= points[d];
    pos[d] = (n / divisor) % points[d];
  }
  return pos;
}
// convert n-dim. coordinate index tuple (i, j, k) into 1d-index-number
int Mesh::pos2index(const std::vector<int> &pos) {
  int n = 0;
  int mul = npoints;
  for (int d = 0; d < rank; d++) {
    mul /= points[d];
    n += pos[d] * mul;
  }
  return n;
}