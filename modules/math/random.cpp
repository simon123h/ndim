#include "random.hpp"
#include <random>

namespace math {
int randomSeed = 999;
int randomSeedBackup = -1;
std::mt19937 gen(randomSeed);
std::uniform_real_distribution<> dis(0.0, 1.0);
// did randomSeed change?
void updateRandomGenerator() {
  if (randomSeed != randomSeedBackup) {
    gen = std::mt19937(randomSeed);
    randomSeedBackup = randomSeed;
  }
}
// simple random double/complex overloads
double random() {
  updateRandomGenerator();
  return dis(gen);
}
template <> double random<double>() { return random(); }
template <> complex random<complex>() {
  return std::polar(random(0., 1.), random(0., 2 * math::PI));
}
double random(double from, double to) {
  return from + (to - from) * random<double>();
}
double randInt(int from, int to) { return floor(random(from, to + 1)); }

// overloaded vector functions of random numbers
std::vector<double> randoms(int N, double amp) {
  std::vector<double> result(N);
  for (int i = 0; i < N; i++)
    result[i] = random(-amp, amp);
  return result;
}
template <> std::vector<double> randoms<double>(int N, double amp) {
  std::vector<double> result(N);
  for (int i = 0; i < N; i++)
    result[i] = random(-amp, amp);
  return result;
}
template <> std::vector<complex> randoms<complex>(int N, double amp) {
  std::vector<complex> result(N);
  for (int i = 0; i < N; i++)
    result[i] = amp * random<complex>();
  return result;
}
} // namespace math
