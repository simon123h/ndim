#pragma once
#include "math.hpp"

/*
 * some general vector related methods
 */

namespace math {

// element wise absolute of vector
template <typename T> std::vector<double> abs(const std::vector<T> &x);

// element wise real part of vector
std::vector<double> real(const std::vector<complex> &x);
// element wise imaginary part of vector
std::vector<double> imag(const std::vector<complex> &x);

// element sum of a vector
double sum(const std::vector<double> &x);
// element product of a vector
double prod(const std::vector<double> &x);
// norm of a vector
double norm(const std::vector<double> &x);

// reshape vector into matrix
std::vector<std::vector<double>> matrix(const std::vector<double> &x, int Nx,
                                        int Ny);

// transpose a matrix
std::vector<std::vector<double>>
transpose(const std::vector<std::vector<double>> &x);

} // namespace math