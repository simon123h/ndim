#include "math/fft.hpp"
#include "math/math.hpp"
#include "output/output.hpp"
#include <fstream>
#include <iostream>

int main(int argc, char const *argv[]) {

  int N = 256;
  double L = 10 * math::PI;
  double omega = 1;
  std::vector<double> x(N);
  std::vector<double> f(N);

  for (int i = 0; i < N; i++) {
    x[i] = L * i / (double)N;
    f[i] = std::sin(omega * x[i]);
  }

  FFT *fft = new FFT(N);

  std::vector<complex> fk = fft->fft(f);
  std::vector<double> k = fft->fftfreq(L);

  std::ofstream rout("real.dat");
  for (int i = 0; i < N; i++) {
    rout << x[i] << " " << f[i] << std::endl;
  }
  rout.close();
  std::ofstream fout("four.dat");
  for (int i = 0; i < fft->fourierPoints; i++) {
    fout << k[i] << " " << std::abs(fk[i]) << std::endl;
  }
  fout.close();

  int Nx = 4;
  int Ny = 4;
  double Lx = 2 * math::PI;
  double Ly = 2 * math::PI;
  fft = new FFT(std::vector<int>{Nx, Ny, Ny, Ny});
  std::ofstream kout("kvec.dat");
  std::vector<std::vector<double>> mk =
      fft->fftfreq(std::vector<double>{Lx, Ly, Ly, Ly});
  for (int i = 0; i < fft->rank; i++) {
    for (int j = 0; j < fft->fourierPoints; j++) {
      kout << mk[i][j] << " ";
    }
    kout << std::endl;
  }
  kout.close();

  int n_tests_succeeded = 0;
  for (int mode = 0; mode < fft->fourierPoints; mode++) {
    int testmode = fft->pos2index(fft->index2pos(mode));
    if (testmode == mode) {
      n_tests_succeeded++;
    } else {
      std::cout << "/* Test failed for mode " << mode << " */" << std::endl;
    }
  }
  std::cout << n_tests_succeeded << " / " << fft->fourierPoints
            << " tests succeeded" << std::endl;

  return 0;
}
