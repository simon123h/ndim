#include "math.hpp"
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <iostream>

// like pythons numpy.linspace
std::vector<double> math::linspace(double from, double to, int num) {
  std::vector<double> result(num);
  for (int i = 0; i < num; i++)
    result[i] = from;
  for (int i = 1; i < num; i++)
    result[i] += (i / (double)(num - 1)) * (to - from);
  return result;
}

// like pythons numpy.arange
std::vector<double> math::range(double from, double to, double step) {
  return linspace(from, to, 1 + (to - from) / step);
}

// like pythons numpy.logspace
std::vector<double> math::logspace(double from, double to, int num,
                                   double base) {
  std::vector<double> result = linspace(from, to, num);
  for (int i = 0; i < num; i++)
    result[i] = exp(log(base) * result[i]);
  return result;
}

// calculate binomial coefficients
int math::binomialCoeff(int n, int k) {
  int res = 1;
  if (k > n - k)
    k = n - k;
  for (int i = 0; i < k; ++i) {
    res *= (n - i);
    res /= (i + 1);
  }
  return res;
}

// convert STL vector to Eigen::Vector
Eigen::VectorXd math::eigenVec(const std::vector<double> &A) {
  Eigen::VectorXd result(A.size());
  for (unsigned i = 0; i < A.size(); i++)
    result(i) = A[i];
  return result;
}
// convert STL matrix to Eigen::Matrix
Eigen::MatrixXd math::eigenMat(const std::vector<std::vector<double>> &A) {
  Eigen::MatrixXd result(A.size(), A[0].size());
  for (unsigned i = 0; i < A.size(); i++)
    for (unsigned j = 0; j < A[0].size(); j++)
      result(i, j) = A[i][j];
  return result;
}

// solve a linear system Ax=b
std::vector<double>
math::linear_solve(const std::vector<std::vector<double>> &A,
                   const std::vector<double> &b) {
  Eigen::MatrixXd eA = eigenMat(A);
  Eigen::VectorXd eb = eigenVec(b);
  Eigen::VectorXd es = eA.colPivHouseholderQr().solve(eb);
  // Eigen::VectorXd es = eA.householderQr().solve(eb);
  return std::vector<double>(es.data(), es.data() + es.size());
}

// sign of determinant of a matrix
double math::det(const std::vector<std::vector<double>> &A) {
  Eigen::MatrixXd eA = eigenMat(A);
  return eA.determinant();
}

// sign of determinant of a matrix
double math::logAbsDet(const std::vector<std::vector<double>> &A) {
  Eigen::MatrixXd eA = eigenMat(A);
  return eA.householderQr().logAbsDeterminant();
}

// sign of determinant of a matrix
double math::det_sign(const std::vector<std::vector<double>> &A) {
  return det_sign(eigenMat(A));
}
// sign of determinant of a matrix (Eigen version)
double math::det_sign(const Eigen::MatrixXd &A) {
  // using LU decomposition
  auto lu = A.fullPivLu();
  Eigen::VectorXd diag = lu.matrixLU().diagonal();
  for (unsigned i = 0; i < diag.size(); i++)
    diag(i) = diag(i) / std::abs(diag(i));
  return lu.permutationQ().determinant() * lu.permutationP().determinant() *
         diag.prod();
}

// calculate eigenvalues of a matrix
std::vector<complex>
math::eigenvalues(const std::vector<std::vector<double>> &A) {
  return eigenvalues(eigenMat(A));
}
// calculate eigenvalues of a matrix (Eigen version)
std::vector<complex> math::eigenvalues(const Eigen::MatrixXd &A) {
  Eigen::EigenSolver<Eigen::MatrixXd> es(A);
  Eigen::VectorXcd ev = es.eigenvalues();
  return std::vector<complex>(ev.data(), ev.data() + ev.size());
}
