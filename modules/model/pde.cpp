#include "pde.hpp"
#include "output/output.hpp"
#include <fstream>
#include <iostream>

// helper function: convert vector of gridpoints to dimension integer
int calcDimension(const std::vector<int> &points) {
  int dim = 1;
  for (unsigned i = 0; i < points.size(); i++)
    dim *= points[i];
  return dim;
}

// construct n-dim. PDE
PDE::PDE(const std::vector<double> &size, const std::vector<int> &points)
    : ODE(calcDimension(points)) {
  // generate equidistant mesh
  mesh = new Mesh(size, points);
  rank = mesh->rank;
}
// construct 1-dim. PDE
PDE::PDE(double L, int N) : PDE(std::vector<double>{L}, std::vector<int>{N}) {}

// destructor
PDE::~PDE() { delete mesh; }

// return the field variables in the spatial domain
std::vector<double> PDE::getField() { return vars; }
// set the field variables in the spatial domain
void PDE::setField(const std::vector<double> &field) { this->vars = field; }
// return the time derivative of the field in the spatial domain
std::vector<double> PDE::getSpatialTimeDerivative() { return rhs(); }

// save the PDE's configuration to a file
void PDE::save(std::string fname) {
  std::ofstream out(fname, std::ios::app);
  out << "rank = " << rank << "\n";
  out << "points = \"";
  for (unsigned i = 0; i < mesh->points.size(); i++) {
    out << mesh->points[i];
    if (i < mesh->points.size() - 1)
      out << " ";
  }
  out << "\"\n";
  out << "size = \"";
  for (unsigned i = 0; i < mesh->size.size(); i++) {
    out << mesh->size[i];
    if (i < mesh->size.size() - 1)
      out << " ";
  }
  out << "\"\n";

  out.close();
  Model::save(fname);
}
