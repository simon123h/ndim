#pragma once
#include "math/mesh.hpp"
#include "pde.hpp"

/*
 * Base class for Finite Differences representations of PDEs, de facto turning
 * them into ODE's (therefore inheritance).
 */

class FiniteDifferencesPDE : public PDE {
private:
public:
  FiniteDifferencesPDE(const std::vector<double> &sizes,
                       const std::vector<int> &gridpoints);
  FiniteDifferencesPDE(double L, int N);
  virtual ~FiniteDifferencesPDE();
  // calculate the n-th derivative of a field on the mesh
  virtual std::vector<double> ddx(const std::vector<double> &field,
                                  int dimension, int degree);
  // return the spatial derivatives of the field
  std::vector<std::vector<double>> getSpatialDerivatives();
};