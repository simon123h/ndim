#include "pde_fd.hpp"
#include "output/output.hpp"
#include <iostream>

// construct n-dim. PDE
FiniteDifferencesPDE::FiniteDifferencesPDE(const std::vector<double> &sizes,
                                           const std::vector<int> &gridpoints)
    : PDE(sizes, gridpoints) {}
// construct 1-dim. FiniteDifferencesPDE
FiniteDifferencesPDE::FiniteDifferencesPDE(double L, int N)
    : PDE(std::vector<double>{L}, std::vector<int>{N}) {}

// destructor
FiniteDifferencesPDE::~FiniteDifferencesPDE() {}

// calculate the n-th derivative of the vars
std::vector<double> FiniteDifferencesPDE::ddx(const std::vector<double> &field,
                                              int dimension, int degree) {

  int d = dimension;
  // get position indices of gridpoints
  std::vector<std::vector<int>> indices = mesh->indices();
  // dimension and result vector
  int size = mesh->points[d];
  std::vector<double> result(field.size());

  // spatial difference from mesh, TODO: generalize for arbitrary grids?
  double dx = mesh->x[0][1] - mesh->x[0][0];
  // calculate derivative for each gridpoint
  for (unsigned n = 0; n < field.size(); n++) {
    // sum formula for central difference
    for (int i = 0; i <= degree; i++) {
      std::vector<int> ind = indices[n];
      int s = i > degree / 2 && degree % 2 == 1 ? 0 : 1;
      // out-of-bound indices lead to fictitious values --> no-flux BCs
      ind[d] = std::max(0, std::min(ind[d] + degree / 2 - i + s, size - 1));
      int index = mesh->pos2index(ind);
      result[n] +=
          std::pow(-1., i) * math::binomialCoeff(degree, i) * field[index];
    }
    // TODO: fix this
    if (degree % 2 == 0)
      result[n] /= std::pow(dx, degree);
    else
      result[n] /= std::pow(dx, 1) * (1. + degree % 2);
  }
  return result;
}

// return the spatial derivatives of the field
std::vector<std::vector<double>> FiniteDifferencesPDE::getSpatialDerivatives() {
  std::vector<std::vector<double>> result(rank,
                                          std::vector<double>(mesh->npoints));
  for (int d = 0; d < rank; d++)
    result[d] = ddx(getField(), d, 1);
  return result;
}
