#include "pde_ps.hpp"
#include <algorithm>

// construct n-dim. pseudospectral PDE
PseudospectralPDE::PseudospectralPDE(const std::vector<double> &sizes,
                                     const std::vector<int> &gridpoints)
    : PDE(sizes, gridpoints) {
  // generate FFT object
  fft = new FFT(gridpoints);
  // generate k-vector
  k = fft->fftfreq(sizes);
  // copy number of discretization points form FFT object
  fourierPoints = fft->fourierPoints;
  realPoints = fft->realPoints;
  // dimension of vars array is defined by number of complex fourier variables
  if (rank > 2)
    setDimension(2 * fourierPoints);
}

// return variables in Fourier domain
std::vector<complex> PseudospectralPDE::getFourier() { return vars2four(vars); }
// return variables in spatial domain
std::vector<double> PseudospectralPDE::getField() {
  std::vector<complex> fvars = getFourier();
  return fft->ifft(fvars);
}
// set variables in Fourier domain
void PseudospectralPDE::setFourier(const std::vector<complex> &vars) {
  setVars(four2vars(vars));
}
// set variables in spatial domain
void PseudospectralPDE::setField(const std::vector<double> &vars) {
  std::vector<double> x = vars;
  setVars(four2vars(fft->fft(x)));
}

// return the time derivative of the field in the spatial domain
std::vector<double> PseudospectralPDE::getSpatialTimeDerivative() {
  std::vector<complex> four = rhs(getFourier());
  return fft->ifft(four);
}
// return the spatial derivatives of the field
std::vector<std::vector<double>> PseudospectralPDE::getSpatialDerivatives() {
  std::vector<std::vector<double>> spatialDerivatives(rank);
  std::vector<complex> four = getFourier();
  for (int d = 0; d < rank; d++) {
    std::vector<complex> tmp(fourierPoints);
    for (int i = 0; i < fourierPoints; i++)
      tmp[i] = math::I * four[i] * k[d][i];
    spatialDerivatives[d] = fft->ifft(tmp);
  }
  return spatialDerivatives;
}

// construct 1-dim. pseudospectral PDE
PseudospectralPDE::PseudospectralPDE(double L, int N)
    : PDE(std::vector<double>{L}, std::vector<int>{N}) {}

PseudospectralPDE::~PseudospectralPDE() { delete fft; }

// generic rhs methods that call their complex equivalents
// usually should not be overwritten
// the linear part of the rhs
std::vector<double> PseudospectralPDE::linear(const std::vector<double> &in,
                                              double t) {
  std::vector<complex> _in = vars2four(in);
  std::vector<complex> out = linear(_in, t);
  return four2vars(out);
}
// the nonlinear part of the rhs
std::vector<double> PseudospectralPDE::nonlinear(const std::vector<double> &in,
                                                 double t) {
  std::vector<complex> _in = vars2four(in);
  std::vector<complex> out = nonlinear(_in, t);
  return four2vars(out);
}
// the governing equation
std::vector<double> PseudospectralPDE::rhs(const std::vector<double> &in,
                                           double t) {
  std::vector<complex> _in = vars2four(in);
  std::vector<complex> out = rhs(_in, t);
  return four2vars(out);
}

// the linear part of the rhs in complex Fourier domain
std::vector<complex> PseudospectralPDE::linear(const std::vector<complex> &in,
                                               double t) {
  // by default everything is regarded nonlinear
  return std::vector<complex>(dimension, 0.);
}
// the nonlinear part of the rhs in complex Fourier domain
std::vector<complex>
PseudospectralPDE::nonlinear(const std::vector<complex> &in, double t) {
  // by default everything is regarded nonlinear
  return rhs(in, t);
}

// convert variables from Fourier domain to degrees-of-freedom domain
// TODO: n-dimensionalize
std::vector<double>
PseudospectralPDE::four2vars(const std::vector<complex> &four) {
  std::vector<double> result(dimension);
  if (rank == 1) {
    int pos = 0;
    int N = mesh->points[0];
    for (int i = 0; i < N / 2 + 1; i++) {
      if (std::find(ignoreModesReal.begin(), ignoreModesReal.end(), i) !=
          ignoreModesReal.end())
        continue;
      result[pos++] = four[i].real();
      if (i == 0 || i == N / 2) // exclude zero- and Nyquist modes
        continue;
      if (std::find(ignoreModesImag.begin(), ignoreModesImag.end(), i) !=
          ignoreModesImag.end())
        continue;
      result[pos++] = four[i].imag();
    }
  } else if (rank == 2) {
    int pos = 0;
    int Nx = mesh->points[0];
    int Ny = mesh->points[1];
    for (int i = 0; i < Nx; i++) {
      for (int j = 0; j < Ny / 2 + 1; j++) {
        if (i > Nx / 2 &&
            (j == 0 || j == Ny / 2)) // exclude complex conjugated modes
          continue;
        int ind = i * (Ny / 2 + 1) + j;
        if (std::find(ignoreModesReal.begin(), ignoreModesReal.end(), ind) !=
            ignoreModesReal.end())
          continue;
        result[pos++] = four[ind].real();
        if ((i == 0 || i == Nx / 2) &&
            (j == 0 || j == Ny / 2)) // exclude zero- and Nyquist modes
          continue;
        if (std::find(ignoreModesImag.begin(), ignoreModesImag.end(), ind) !=
            ignoreModesImag.end())
          continue;
        result[pos++] = four[ind].imag();
      }
    }
  } else {
    for (unsigned i = 0; i < four.size(); i++) {
      result[2 * i] = four[i].real();
      result[2 * i + 1] = four[i].imag();
    }
  }
  return result;
}

// convert variables from degrees-of-freedom domain to Fourier domain
// TODO: this costs ~10% of the performance, while the n-d-case doesn't
std::vector<complex>
PseudospectralPDE::vars2four(const std::vector<double> &vars) {
  std::vector<complex> result(fft->fourierPoints);
  if (rank == 1) {
    int pos = 0;
    int N = mesh->points[0];
    for (int i = 0; i < N / 2 + 1; i++) {
      if (std::find(ignoreModesReal.begin(), ignoreModesReal.end(), i) !=
          ignoreModesReal.end())
        continue;
      result[i] = complex(vars[pos++], 0);
      if (i == 0 || i == N / 2) // exclude zero- and Nyquist modes
        continue;
      if (std::find(ignoreModesImag.begin(), ignoreModesImag.end(), i) !=
          ignoreModesImag.end())
        continue;
      result[i] += complex(0, vars[pos++]);
    }
  } else if (rank == 2) {
    int pos = 0;
    int Nx = mesh->points[0];
    int Ny = mesh->points[1];
    for (int i = 0; i < Nx; i++) {
      for (int j = 0; j < Ny / 2 + 1; j++) {
        if (i > Nx / 2 &&
            (j == 0 || j == Ny / 2)) // exclude complex conjugated modes
          continue;
        int ind = i * (Ny / 2 + 1) + j;
        if (std::find(ignoreModesReal.begin(), ignoreModesReal.end(), ind) !=
            ignoreModesReal.end())
          continue;
        result[ind] = vars[pos++];
        if ((i == 0 || i == Nx / 2) &&
            (j == 0 || j == Ny / 2)) // exclude zero- and Nyquist modes
          continue;
        if (std::find(ignoreModesImag.begin(), ignoreModesImag.end(), ind) !=
            ignoreModesImag.end())
          continue;
        result[ind] += vars[pos++] * math::I;
      }
    }
    // restore complex conjugated values
    for (int i = Nx / 2; i < Nx; i++) {
      result[i * (Ny / 2 + 1) + 0] =
          std::conj(result[(Nx - i) * (Ny / 2 + 1) + 0]);
      result[i * (Ny / 2 + 1) + Ny / 2] =
          std::conj(result[(Nx - i) * (Ny / 2 + 1) + Ny / 2]);
    }
  } else {
    for (int i = 0; i < fft->fourierPoints; i++) {
      result[i] = complex(vars[2 * i], vars[2 * i + 1]);
    }
  }
  return result;
}

// reset the zero mode to zero
void PseudospectralPDE::resetZeroMode() {
  std::vector<complex> four = getFourier();
  four[0] = complex(0, 0);
  setFourier(four);
}

// multipy v(k) with e^ikz, with z so that v(k)[mode].imag() == 0
// results in lateral shift of the field
void PseudospectralPDE::resetLateralShift(int mode) {
  std::vector<complex> v = getFourier();
  for (int d = 0; d < rank; d++)
    for (unsigned i = 0; i < k[d].size(); i++)
      k[d][i] *= mesh->size[d];
  double kabs = 0;
  std::vector<double> phase(rank);
  for (int d = 0; d < rank; d++)
    kabs += fabs(k[d][mode]);
  for (int d = 0; d < rank; d++) {
    int sgn = k[d][mode] < 0 ? -1 : (k[d][mode] > 0) ? 1 : 0;
    phase[d] = -sgn * atan2(v[mode].imag(), v[mode].real()) / kabs;
  }
  for (int d = 0; d < rank; d++)
    for (int i = 0; i < fourierPoints; i++)
      v[i] *= exp(math::I * k[d][i] * phase[d]);
  setFourier(v);
}