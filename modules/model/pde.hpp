#pragma once
#include "math/math.hpp"
#include "math/mesh.hpp"
#include "ode.hpp"
#include <cmath>

/*
 * Base class for partial differential equations of arbitrary dimension,
 * simulated on domains specified my the Mesh variable.
 * Elaborate Equation types may inherit from the PDE class and thus provide
 * various ways to deal with PDE's. Hence, this class remains mostly abstract.
 */

class PDE : public ODE {
private:
public:
  PDE(const std::vector<double> &size, const std::vector<int> &points);
  PDE(double L, int N);
  virtual ~PDE();
  // the spatial dimension of the PDE
  int rank;
  // discrete spatial mesh of the domain
  Mesh *mesh;

  // return the field variables in the spatial domain
  virtual std::vector<double> getField();
  // set the field variables in the spatial domain
  virtual void setField(const std::vector<double> &field);
  // return the time derivative of the field in the spatial domain
  virtual std::vector<double> getSpatialTimeDerivative();
  // return the spatial derivatives of the field
  virtual std::vector<std::vector<double>> getSpatialDerivatives() = 0;

  // save the PDE's configuration to a file
  virtual void save(std::string fname);
};