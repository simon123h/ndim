#pragma once

#include "model.hpp"

/*
 * Base class for ordinary differential equations of arbitrary dimension.
 * ODE's also are an important interface as PDE's are reduced to ODE's via
 * various schemes (Pseudospectral, FiniteDifferences, FEM, ...)
 */

class ODE : public Model {
private:
public:
  ODE(int dimension);
  virtual ~ODE();

  // ODE's and inheriting classes may split their rhs into a linear and a
  // nonlinear part, as it is useful to some integration schemes or the like.
  // By default, the whole rhs is considered to be nonlinear
  virtual std::vector<double> linear(const std::vector<double> &in,
                                     double t = 0);
  virtual std::vector<double> nonlinear(const std::vector<double> &in,
                                        double t = 0);
};