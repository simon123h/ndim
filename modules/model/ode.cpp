#include "ode.hpp"

ODE::ODE(int dimension) : Model(dimension) {}

ODE::~ODE() {}

// the linear part of the rhs
std::vector<double> ODE::linear(const std::vector<double> &in, double t) {
  return std::vector<double>(dimension, 0.);
}
// the nonlinear part of the rhs
std::vector<double> ODE::nonlinear(const std::vector<double> &in, double t) {
  return rhs(in, t);
}
