#pragma once
#include <string>
#include <vector>

/*
 * Base class for any model to be investigated.
 * This class covers the functionality of a dynamical system and serves
 * as a general interface for many other classes of models to inherit from,
 * e.g.: Model (Dynamical System) --> ODE --> PDE --> ... --> MyEquation
 */

class Model {
private:
public:
  Model(int dimension);
  virtual ~Model();

  // the number of degrees of freedom
  int dimension;

  // the variables
  std::vector<double> vars;

  // the parameters
  std::vector<double *> parameters;

  // the time variable
  double t = 0;

  // is the model a dynamical system with discrete time?
  bool is_time_discrete = false;

  // the governing equation
  virtual std::vector<double> rhs(const std::vector<double> &vars,
                                  double t = 0) = 0;
  // also callable without argument
  virtual std::vector<double> rhs();

  // the jacobian of the rhs
  virtual std::vector<std::vector<double>>
  jacobian(const std::vector<double> &vars, double t = 0);
  // also callable without argument
  virtual std::vector<std::vector<double>> jacobian();

  // getters/setters
  virtual std::vector<double> getVars();
  virtual void setVars(const std::vector<double> &vars);
  virtual void setDimension(int dimension);

  // save the model's configuration to a file
  // TODO: rename?
  virtual void save(std::string fname);
  // TODO: 'load'-equivalent to save?
};