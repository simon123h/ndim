#include "model.hpp"
#include <fstream>

// construct a model
Model::Model(int dimension) : dimension(dimension) {
  vars = std::vector<double>(dimension);
}

// destructor
Model::~Model() {}

// set the phase space variables
void Model::setVars(const std::vector<double> &vars) { this->vars = vars; }
// return the phase space variables
std::vector<double> Model::getVars() { return vars; }
// update the dimension of the model
void Model::setDimension(int dimension) {
  this->dimension = dimension;
  vars.resize(dimension);
}

// rhs() without arguments returns rhs of current vars vector
std::vector<double> Model::rhs() { return rhs(vars); }

// naive computation of the jacobian via finite difference method
std::vector<std::vector<double>>
Model::jacobian(const std::vector<double> &vars, double t) {
  std::vector<std::vector<double>> jac(dimension,
                                       std::vector<double>(dimension));
  // size of the finite difference
  double h = 1e-10;
  // the unperturbed value of the rhs
  std::vector<double> rhsorig = rhs(vars, t);
  // wrt. each dimension <d>
  for (int d = 0; d < dimension; d++) {
    // deviate vars by <h> in dimension <d> and evaluate rhs
    std::vector<double> deviated = vars;
    deviated[d] += h;
    deviated = rhs(deviated, t);
    // calculate the vector of derivatives
    for (int i = 0; i < dimension; i++)
      jac[i][d] = (deviated[i] - rhsorig[i]) / h;
  }
  return jac;
}
// jacobian() without arguments returns jacobian of current vars vector
std::vector<std::vector<double>> Model::jacobian() { return jacobian(vars); }

// save the model's configuration to a file
void Model::save(std::string fname) {
  std::ofstream out(fname, std::ios::app);
  out << "dimension = " << dimension << "\n";
  out << "parameters = \"";
  for (unsigned i = 0; i < parameters.size(); i++) {
    out << *parameters[i];
    if (i < parameters.size() - 1)
      out << " ";
  }
  out << "\"\n";
  out.close();
}