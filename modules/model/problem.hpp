#incl /de "model/model.hpp"
#include "solver/solver.hpp"
#include "stepper/stepper.hpp"

class Problem {
private:
public:
  Problem();
  virtual ~Problem();

  // the model of interest
  Model *model;

  // the time-stepper, integrates in time
  Stepper *stepper;

  // the solver, finds solution via
  Solver *solver;

  // call the stepper
  void step();

  // call the solver
  void solve();
};

Problem::Problem() {}

Problem::~Problem() {}

void Problem::step() { stepper->step(); }

void Problem::solve() { solver->solve(); }