#pragma once
#include "math/fft.hpp"
#include "math/math.hpp"
#include "pde.hpp"

/*
 * Base class for pseudospectral representations of PDEs, de facto turning them
 * into ODE's (therefore inheritance).
 * PseudospectralPDEs have their field internally represented in a complex
 * discrete Fourier space, that is mapped onto the real vars vector of the
 * general Model class. They thus need their rhs formulated in the Fourier
 * domain. Also, they provide elaborate tools to deal with Fourier space, most
 * prominently the FFT object for Fourier transforms and the k-vector for
 * derivatives.
 */

class PseudospectralPDE : public PDE {
private:
public:
  PseudospectralPDE(const std::vector<double> &sizes,
                    const std::vector<int> &gridpoints);
  PseudospectralPDE(double L, int N);
  virtual ~PseudospectralPDE();

  // number of discretization points in Fourier and spatial domains
  int fourierPoints;
  int realPoints;
  // FFT object providing Fast Fourier Transformation methods
  FFT *fft;
  // k-vector of discrete frequencies
  std::vector<std::vector<double>> k;
  // list of modes to leave out in vars2four methods
  std::vector<int> ignoreModesReal{};
  std::vector<int> ignoreModesImag{};

  // add getters/setters for Fourier and spatial domain variables
  virtual std::vector<complex> getFourier();
  virtual std::vector<double> getField();
  virtual void setFourier(const std::vector<complex> &vars);
  virtual void setField(const std::vector<double> &vars);

  // return the time derivative of the field in the spatial domain
  virtual std::vector<double> getSpatialTimeDerivative();
  // return the spatial derivatives of the field
  virtual std::vector<std::vector<double>> getSpatialDerivatives();

  // generic rhs methods that call their complex equivalents
  virtual std::vector<double> linear(const std::vector<double> &in,
                                     double t = 0);
  virtual std::vector<double> nonlinear(const std::vector<double> &in,
                                        double t = 0);
  virtual std::vector<double> rhs(const std::vector<double> &in, double t = 0);

  // actual rhs, linear and nonlinear part of complex Fourier variables
  virtual std::vector<complex> linear(const std::vector<complex> &in,
                                      double t = 0);
  virtual std::vector<complex> nonlinear(const std::vector<complex> &in,
                                         double t = 0);
  virtual std::vector<complex> rhs(const std::vector<complex> &in,
                                   double t = 0) = 0;

  // Convert fourier variables to vars arrays and back.
  // This is necessary, because calculation happens in the complex fourier
  // domain, whereas the vars array is always of type double.
  // also the fourier variables contain redundancies due to R2C symmetry
  // for the vars array, these redundancies are removed
  virtual std::vector<double> four2vars(const std::vector<complex> &in);
  virtual std::vector<complex> vars2four(const std::vector<double> &in);

  // reset the zero mode to zero
  virtual void resetZeroMode();

  // multipy v(k) with e^ikz, with z so that v(k)[mode].imag() == 0
  // results in lateral shift of the field
  virtual void resetLateralShift(int mode);
};