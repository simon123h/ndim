#include "output.hpp"
#include <cstring>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <sys/stat.h>

namespace output {

/*
 * Filesystem operations
 */

// create a directory, if ot doesn't exist
void mkdir(std::string dir) {
  std::experimental::filesystem::create_directories(dir);
  // system(("mkdir -p " + dir).c_str());
}
// create multiple directories
void mkdirs(std::vector<std::string> dirs) {
  for (std::string dir : dirs)
    mkdir(dir);
}

bool path_exists(const std::string &name) {
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}
// remove everything in output folder ./out/
void clearOut() {
  system("rm -rf out");
  // if(std::experimental::filesystem::exists("out"))
  // std::experimental::filesystem::remove_all("out");
  mkdir("out");
}

/*
 * Save stuff
 */

// save single value to ofstream
void save(std::ofstream &fstream, double x, bool transpose) {
  fstream << std::setprecision(precision);
  fstream << x;
  if (transpose)
    fstream << " ";
  else
    fstream << std::endl;
}
// save single value to file <fname>
void save(const std::string &fname, double x, bool transpose) {
  std::ofstream out(fname, std::ios::app);
  save(out, x, transpose);
  out.close();
}
// save single value to file <fname> and prepend comment
void saveAs(const std::string &fname, const std::string &comment, double x,
            bool transpose) {
  save(fname, comment);
  save(fname, x, transpose);
}
// also for other types
void save(const std::string &fname, int x, bool transpose) {
  std::ofstream out(fname, std::ios::app);
  save(out, (double)x, transpose);
  out.close();
}
void save(const std::string &fname, const std::string &x) {
  std::ofstream out(fname, std::ios::app);
  out << x;
  out.close();
}
void save(const std::string &fname, const char *x) {
  save(fname, std::string(x));
}

// save 1d vectors to ofstream
void save(std::ofstream &fstream, const std::vector<double> &x,
          bool transpose) {
  for (unsigned i = 0; i < x.size(); i++)
    save(fstream, x[i], transpose && (i < x.size() - 1));
}
// save 1d vectors to file <fname>
void save(const std::string &fname, const std::vector<double> &x,
          bool transpose) {
  std::ofstream out(fname, std::ios::app);
  save(out, x, transpose);
  out.close();
}

// save 2d vectors to ofstream
void save(std::ofstream &fstream, const std::vector<std::vector<double>> &x,
          bool transpose) {
  if (!transpose) {
    for (unsigned i = 0; i < x.size(); i++)
      save(fstream, x[i], true);
  } else {
    for (unsigned i = 0; i < x[0].size(); i++) {
      for (unsigned j = 0; j < x.size(); j++)
        save(fstream, x[j][i], true);
      fstream << std::endl;
    }
  }
}
// save 2d vectors to the file <fname>
void save(const std::string &fname, const std::vector<std::vector<double>> &x,
          bool transpose) {
  std::ofstream out(fname, std::ios::app);
  save(out, x, transpose);
  out.close();
}

// save field values on point grid, useful for spatial and Fourier output
void save(const std::string &fname, const std::vector<std::vector<double>> &x,
          const std::vector<double> &f) {
  // copy grid vector, so it won't be altered outside
  std::vector<std::vector<double>> vec = x;
  // merge field vector into grid vector
  vec.push_back(f);
  // save as transposed --> column-wise output
  save(fname, vec, true);
}

// save n-dim. fourier space together with k-vector in correct order
void saveFourier(const std::string &fname,
                 const std::vector<std::vector<double>> &k,
                 const std::vector<complex> &f, const std::vector<int> points) {
  std::ofstream out(fname);
  int rank = k.size();
  if (rank == 1) {
    int N = f.size();
    for (int i = 0; i < N; i++)
      out << k[0][i] << ' ' << std::abs(f[i]) << std::endl;
  } else if (rank == 2) {
    int Nx = points[0];
    int Ny = points[1] / 2 + 1;
    for (int i = 0; i < Nx - 1; i++) {
      for (int j = 0; j < Ny; j++) {
        int index = (i < Nx / 2 - 1) ? (i + Nx / 2 + 1) * Ny + j
                                     : (i - (Nx / 2 - 1)) * Ny + j;
        out << k[0][index] << ' ' << k[1][index] << ' ' << std::abs(f[index])
            << std::endl;
      }
    }
  } else {
    save(fname, k, math::abs(f));
  }
  out.close();
}

/*
 * Load stuff
 */

// load single value from ifstream
void load(std::ifstream &fstream, double &x) { fstream >> x; }
// load single value from file <fname>
void load(const std::string &fname, double &x) {
  std::ifstream in(fname);
  load(in, x);
  in.close();
}
// load single value from file <fname>
double loadValue(const std::string &fname) {
  double x;
  load(fname, x);
  return x;
}

// load vector of values from ifstream
void load(std::ifstream &fstream, std::vector<double> &x, int column) {
  std::vector<double> data(0);
  x = std::vector<double>(0);
  double v;
  if (column == -1) {
    // load all values into vector
    while (fstream >> v)
      x.push_back(v);
  } else {
    // load only specific column into vector
    std::vector<std::vector<double>> data(0);
    load(fstream, data, true);
    if (data.size() > column)
      x = data[column];
  }
}
// load vector of values from file <fname>
void load(const std::string &fname, std::vector<double> &x, int column) {
  std::ifstream in(fname);
  load(in, x, column);
  in.close();
}
// load vector of values from file <fname>
std::vector<double> load(const std::string &fname, int column) {
  std::vector<double> x(0);
  load(fname, x, column);
  return x;
}

// load matrix of values from ifstream
void load(std::ifstream &fstream, std::vector<std::vector<double>> &x,
          bool transpose) {
  x = std::vector<std::vector<double>>(0);
  double v;
  // split into lines
  std::string line;
  while (std::getline(fstream, line)) {
    std::istringstream lineStream(line);
    // read line into vector
    std::vector<double> lineData;
    while (lineStream >> v)
      lineData.push_back(v);
    // add line to matrix
    x.push_back(lineData);
  }
  // optional: transpose
  if (transpose) {
    std::vector<std::vector<double>> y(x[0].size(),
                                       std::vector<double>(x.size()));
    for (unsigned i = 0; i < x[0].size(); i++)
      for (unsigned j = 0; j < x.size(); j++)
        y[i][j] = x[j][i];
    x = y;
  }
}
// load matrix of values from file <fname>
void load(const std::string &fname, std::vector<std::vector<double>> &x,
          bool transpose) {
  std::ifstream in(fname);
  load(in, x, transpose);
  in.close();
}
// load matrix of values from file <fname>
std::vector<std::vector<double>> loadMatrix(const std::string &fname,
                                            bool transpose) {
  std::vector<std::vector<double>> x(0);
  load(fname, x, transpose);
  return x;
}

// read multiple vectors at once;
void load(const std::string &fname, std::vector<double> &x,
          std::vector<double> &y) {
  std::vector<std::vector<double>> data = loadMatrix(fname, true);
  x = data[0];
  y = data[1];
}
void load(const std::string &fname, std::vector<double> &x,
          std::vector<double> &y, std::vector<double> &z) {
  std::vector<std::vector<double>> data = loadMatrix(fname, true);
  x = data[0];
  y = data[1];
  z = data[2];
}

/*
 * Helper functions
 */

// truncate a file to zero size
void truncate(const std::string &fname) {
  std::ofstream out(fname, std::ios::trunc);
}
// floating point precision for save functions
int precision = 5;

// TODO: remove
std::vector<double> loadReal(std::string fname, int columns, int column) {
  std::ifstream in(fname);
  std::vector<double> data(0);
  double x;
  int i = 0;
  while (in >> x) {
    if (i % columns == column)
      data.push_back(x);
    i++;
  }
  in.close();
  return data;
}

} // namespace output
