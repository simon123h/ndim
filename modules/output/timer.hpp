/*
   A module providing some utility functions
 */

#pragma once
#include <chrono>
#include <string>
#include <vector>

// a nice name for long expression
typedef std::chrono::high_resolution_clock::time_point timepoint;

namespace timer {
// get current timepoint
timepoint start();
// get duration in microseconds between two timepoints
double stop(timepoint t1);
// get current timestamp
long int timestamp();
// return formatted time
std::string format(std::string fmt = "%d/%m/%Y %X", time_t t = time(NULL));
// return vector of hours, minutes and seconds for given number of seconds
std::vector<int> hhmmss(int seconds);
} // namespace timer

class Timer {
private:
  timepoint starttime;
  int times_called;
  double initialProgress;

public:
  Timer(std::string title = "", int rollback_lines = 0);
  ~Timer();
  // reset the timer
  void reset();
  // the elapsed time since start
  double getTime();
  double t;
  // the progress since start
  double progress;
  // show a fancy progressbar
  int show(double x);
  int length = 60;
  int rollback_lines = 0;
  std::string title;
};

// simple imperative function for showing a progressbar
int progressbar(double progress = -1, std::string title = "",
                int rollback_lines = 0);