/*
   A class wrapper for a separate async plotting thread
 */

#pragma once
#include <cstdio>
#include <iostream>
#include <string>

namespace plotter {

// total plot count
extern int count;
// name of the gnuplot script
extern std::string plotFile;
// give verbose output?
extern bool quiet;
// how many plotting processes in parallel?
extern int parallelPlots;

// possible entry point: start plotting thread
void start();

// adds a plot-ID to the queue, to be plotted
// just call plotter::add(id), implies plotter::start()
void add(int id);

// give finish signal to plotThread and wait for it to join
void finish();

// make a video with avconv
void makeVideo(const char *name = "out.mp4", const char *path = "out/img/");
} // namespace plotter
