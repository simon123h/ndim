#pragma once
#include "math/math.hpp"
#include "math/vector.hpp"
#include <cstring>
#include <iostream>
#include <memory>

/*
 * A module for everything output-related, such as:
 * - filesystem operations
 * - I/O: load/save variables, vectors and matrices
 */

namespace output {

/*
 * Filesystem operations
 */

// create a directory
void mkdir(std::string dir);
void mkdirs(std::vector<std::string> dirs);
bool path_exists(const std::string &name);
// remove everything in output folder ./out/
void clearOut();

/*
 * Save stuff
 */

// save single value to ofstream
void save(std::ofstream &fstream, double x, bool transpose = false);
// save single value to file <fname>
void save(const std::string &fname, double x, bool transpose = false);
// also for other types
void save(const std::string &fname, int x, bool transpose = false);
void save(const std::string &fname, const std::string &x);
void save(const std::string &fname, const char *x);
// save single value to file <fname> and prepend comment
// TODO: rename?
void saveAs(const std::string &fname, const std::string &comment, double x,
            bool transpose = false);

// save 1d vectors to ofstream
// TODO: generic overload so that ints can also be stored
void save(std::ofstream &fstream, const std::vector<double> &x,
          bool transpose = false);
// save 1d vectors to file <fname>
void save(const std::string &fname, const std::vector<double> &x,
          bool transpose = false);

// save 2d vectors to ofstream
void save(std::ofstream &fstream, const std::vector<std::vector<double>> &x,
          bool transpose = false);
// save 2d vectors to the file <fname>
void save(const std::string &fname, const std::vector<std::vector<double>> &x,
          bool transpose = false);

// convert list of args into vector and save it column-wise to file <fname>
template <typename... Ts> void save(const std::string &fname, Ts... args) {
  // expand args into vector
  auto initList = {args...};
  using T = typename decltype(initList)::value_type;
  std::vector<T> vec{initList};
  // save as transposed --> column-wise output
  save(fname, vec, true);
}

// save field values on point grid, useful for spatial and Fourier output
void save(const std::string &fname, const std::vector<std::vector<double>> &x,
          const std::vector<double> &f);

// save n-dim. fourier space together with k-vector in correct order
void saveFourier(const std::string &fname,
                 const std::vector<std::vector<double>> &k,
                 const std::vector<complex> &f, const std::vector<int> points);

/*
 * Load stuff
 */

// load single value from ifstream
void load(std::ifstream &fstream, double &x);
// load single value from file <fname>
void load(const std::string &fname, double &x);
double loadValue(const std::string &fname);

// load vector of values from ifstream
void load(std::ifstream &fstream, std::vector<double> &x, int column = -1);
// load vector of values from file <fname>
void load(const std::string &fname, std::vector<double> &x, int column = -1);
std::vector<double> load(const std::string &fname, int column = -1);

// load matrix of values from ifstream
void load(std::ifstream &fstream, std::vector<std::vector<double>> &x,
          bool transpose = false);
// load matrix of values from file <fname>
void load(const std::string &fname, std::vector<std::vector<double>> &x,
          bool transpose = false);
std::vector<std::vector<double>> loadMatrix(const std::string &fname,
                                            bool transpose = false);

// read multiple vectors at once;
void load(const std::string &fname, std::vector<double> &x,
          std::vector<double> &y);
void load(const std::string &fname, std::vector<double> &x,
          std::vector<double> &y, std::vector<double> &z);
// TODO: remove
std::vector<double> loadReal(std::string fname, int columns = 1, int column = 0);

/*
 * Helper functions
 */

// format a string like sprintf and return it
template <typename... Args>
std::string format(const std::string &format, Args... args) {
  size_t size = snprintf(nullptr, 0, format.c_str(), args...) + 1;
  std::unique_ptr<char[]> buf(new char[size]);
  snprintf(buf.get(), size, format.c_str(), args...);
  return std::string(buf.get(), buf.get() + size - 1);
}

// truncate a file to zero size
void truncate(const std::string &fname);
// floating point precision for save functions
extern int precision;

} // namespace output
