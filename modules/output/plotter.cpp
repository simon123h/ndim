#include "plotter.hpp"
#include <queue>
#include <string>
#include <thread>
#include <unistd.h>
#include <vector>

namespace plotter {

// total plot count
int count;
// name of the gnuplot script
std::string plotFile = "plot.plt";
// give verbose output?
bool quiet = true;
// how many plotting processes in parallel?
int parallelPlots = 1;
// threads for plotting
std::vector<std::thread> plotThreads;
// queue for leftover plots
std::queue<int> queue;
char tmp_str[512];
bool running;

// call gnuplot
void plot(int id) {
  sprintf(tmp_str, "gnuplot -e \"i=%d\" %s", id, plotFile.c_str());
  system(tmp_str);
  if (!quiet)
    std::cout << "Plot " << id << '\n';
}

// look for id's in the queue and try to plot them
// this method can be multi-threaded
void run() {
  while (running) {
    if (!queue.empty()) {
      int id = queue.front();
      queue.pop();
      plot(id);
    } else {
      // sleep for 0.01s
      usleep(0.01 * 1e6);
    }
  }
}

// possible entry point: start plotting thread
void start() {
  std::cout << "Start plotting." << '\n';
  running = true;
  count = 0;
  for (int i = 0; i < parallelPlots; i++)
    plotThreads.push_back(std::thread(run));
}

// adds a plot-ID to the queue, to be plotted
// just call plotter::add(id), implies plotter::start()
void add(int id) {
  if (!running)
    start();
  queue.push(id);
  count++;
}

// give finish signal to plotThread and wait for it to join
void finish() {
  if (!running)
    return;
  while (!queue.empty())
    usleep(0.1 * 1e6);
  running = false;
  for (int i = 0; i < parallelPlots; i++)
    plotThreads[i].join();
  std::cout << "Plotting finished. Plots: " << count << '\n';
}

// make a video with avconv
void makeVideo(const char *name, const char *path) {
  // avconv -r 25 -i 'out/img/%05d.png' -y out.mp4
  sprintf(tmp_str, "avconv -r 25 -i '%s%%05d.png' -y %s 2> /dev/null", path,
          name);
  system(tmp_str);
  // std::cout << "Video created." << '\n';
}
} // namespace plotter
