
#include "timer.hpp"
#include <cmath>
#include <iostream>
#include <string>
using namespace std::chrono;

namespace timer {
// get current timepoint
timepoint start() { return high_resolution_clock::now(); }
// get duration in microseconds since timepoint t1
double stop(timepoint t1) {
  return (double)duration_cast<microseconds>(start() - t1).count() / 1000000.;
}
// get current UNIX timestamp
long int timestamp() {
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch())
      .count();
}
// return formatted time
std::string format(std::string fmt, time_t t) {
  struct tm tstruct;
  char buf[40];
  tstruct = *localtime(&t);
  strftime(buf, sizeof(buf), fmt.c_str(), &tstruct);
  return buf;
}
// return vector of hours, minutes and seconds for given number of seconds
std::vector<int> hhmmss(int seconds) {
  int hours = seconds / 3600;
  seconds -= 3600 * hours;
  int minutes = seconds / 60;
  seconds -= 60 * minutes;
  return std::vector<int>{hours, minutes, seconds};
}
} // namespace timer

Timer::Timer(std::string title, int rollback_lines)
    : rollback_lines(rollback_lines), title(title) {
  reset();
}

Timer::~Timer() {}

void Timer::reset() {
  starttime = timer::start();
  times_called = 0;
  progress = 0;
  t = 0;
}

double Timer::getTime() {
  t = timer::stop(starttime);
  return t;
}

int Timer::show(double progress) {
  this->progress = progress;
  std::cout << '\n';
  // erase previous lines
  if (times_called > 0)
    for (int i = 0; i < rollback_lines; i++)
      std::cout << "\e[A\r";
  // optionally print title
  if (title != "")
    std::cout << title << '\n';
  // make progress stick to 100%, prevents e.g. 100.05%
  if (progress > 0.995 && progress < 1.005)
    progress = 1;
  // print actual bar
  std::cout << "[";
  int pos = length * progress;
  for (int i = 0; i < length; ++i) {
    if (i < pos)
      std::cout << "=";
    else if (i == pos)
      std::cout << ">";
    else
      std::cout << " ";
  }
  std::cout << "]\n";
  // calculate and format elapsed time and ETA
  t = getTime();
  std::vector<int> ela = timer::hhmmss(t);
  std::vector<int> eta(3);
  if (fabs(progress - initialProgress) > 1e-16) {
    eta = timer::hhmmss(
        t * ((1 - initialProgress) / (progress - initialProgress) - 1));
  }
  // print times and percentage
  printf("ETA:%3dh:%2dm:%2ds%4selapsed:%3dh:%2dm:%2ds%*s%6.2f%%\n", eta[0],
         eta[1], eta[2], "", ela[0], ela[1], ela[2], length - 45, "",
         progress * 100);

  return times_called++;
}

Timer *_progressbar_timer = nullptr;
int progressbar(double progress, std::string title, int rollback_lines) {
  if (progress < 0) {
    delete _progressbar_timer;
    _progressbar_timer = nullptr;
  }
  if (_progressbar_timer == nullptr)
    _progressbar_timer = new Timer();
  _progressbar_timer->title = title;
  _progressbar_timer->rollback_lines = rollback_lines;
  if (progress >= 0)
    return _progressbar_timer->show(progress);
  return -1;
}