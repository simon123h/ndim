
#include "math/math.hpp"
#include "output/output.hpp"
#include <iostream>

int main(int argc, char const *argv[]) {

  output::truncate("0d.dat");
  output::truncate("1d.dat");
  output::truncate("1dv.dat");
  output::truncate("2d.dat");
  output::truncate("2dv.dat");
  output::truncate("str.dat");

  std::cout << "/* start 0d */" << std::endl;
  double a = 1.23456789;
  double b = 2.9999999999;
  double c = 3.1111111111;
  double d = 11;
  double e = 12;
  double f = 13;

  output::precision = 11;

  output::save("0d.dat", a);

  std::cout << "/* SAVE STUFF */" << std::endl;
  std::cout << "/* start 1d */" << std::endl;
  std::vector<double> x1{a, b, c};
  std::vector<double> x2{d, e, f};
  output::save("1d.dat", x1);

  std::cout << "/* start 2d */" << std::endl;
  std::vector<std::vector<double>> y{x1, x2};
  output::save("2d.dat", y);

  std::cout << "/* varia 1d */" << std::endl;
  output::save("1dv.dat", a, b, c);

  std::cout << "/* varia 2d */" << std::endl;
  output::save("2dv.dat", x1, x2);

  output::save("str.dat", "myint: ");
  output::save("str.dat", 7);
  output::save("str.dat", "\nthyint: ");
  output::save("str.dat", 3);

  std::cout << "/* LOAD STUFF */" << std::endl;

  // single values
  double la = output::loadValue("0d.dat");
  std::cout << la << std::endl;

  // vectors
  std::vector<double> lx1 = output::load("1d.dat");
  std::vector<double> lx1_2 = output::load("2dv.dat", 0);
  std::vector<double> lx2_2 = output::load("2dv.dat", 1);
  std::vector<double> lx1_3, lx2_3;
  output::load("2dv.dat", lx1_3, lx2_3);

  // matrices
  std::vector<std::vector<double>> matrix = output::loadMatrix("2d.dat");
  std::vector<std::vector<double>> matrixT = output::loadMatrix("2d.dat", true);

  return 0;
}
