#include "drift.hpp"
#include "math/fft.hpp"
#include "math/math.hpp"
#include "model/pde.hpp"
#include <iostream>
#include <vector>

using namespace measure;

// calculate the lateral drift of a field of arbitrary dimension
// (model->rank) according to formula from thesis
std::vector<double> measure::drift(PDE *model) {
  // calculate time derivative
  std::vector<double> timeDerivative = model->getSpatialTimeDerivative();
  // calculate spatial derivatives
  std::vector<std::vector<double>> spatialDerivatives =
      model->getSpatialDerivatives();

  // calculate average of spatial derivatives
  std::vector<double> spatialDerivativesMid(model->rank);
  for (int d = 0; d < model->rank; d++) {
    for (unsigned int i = 0; i < spatialDerivatives[d].size(); i++)
      spatialDerivativesMid[d] += spatialDerivatives[d][i];
    spatialDerivativesMid[d] /= spatialDerivatives[d].size();
  }

  // calculate average of time derivative
  double timeDerivativeMid = 0;
  for (unsigned int i = 0; i < timeDerivative.size(); i++)
    timeDerivativeMid += timeDerivative[i];
  timeDerivativeMid /= timeDerivative.size();

  // create system of linear equations
  std::vector<std::vector<double>> A(model->rank,
                                     std::vector<double>(model->rank));
  std::vector<double> b(model->rank);
  for (int i = 0; i < model->rank; i++) {
    for (int j = 0; j < model->rank; j++) {
      for (unsigned k = 0; k < timeDerivative.size(); k++)
        A[i][j] += (spatialDerivatives[i][k] - spatialDerivativesMid[i]) *
                   spatialDerivatives[j][k];
    }
    for (unsigned k = 0; k < timeDerivative.size(); k++)
      b[i] += -(spatialDerivatives[i][k] - spatialDerivativesMid[i]) *
              timeDerivative[k];
  }

  // solve the linear system and return result
  return math::linear_solve(A, b);
}