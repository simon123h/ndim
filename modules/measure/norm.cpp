#include "norm.hpp"
#include "math/math.hpp"
#include "model/model.hpp"
#include "model/pde_ps.hpp"
#include <vector>

// L2-Norm of variables
double measure::l2norm(Model *model) {
  // square sum of variables
  std::vector<double> v = model->getVars();
  double d = v.size();
  // ...respectively spatial field for PDEs
  PDE *pde = dynamic_cast<PDE *>(model);
  if (pde != nullptr) {
    v = pde->getField();
    d = pde->mesh->npoints / pde->mesh->area;
  }
  // sum up
  double norm = 0;
  for (unsigned i = 0; i < v.size(); i++)
    norm += pow(v[i], 2.);
  return sqrt(norm / d);
}

// roughness of variables
double measure::roughness(Model *model) {
  // calculate standard deviation of variables
  std::vector<double> v = model->getVars();
  // ...respectively spatial field for PDEs
  PDE *pde = dynamic_cast<PDE *>(model);
  if (pde != nullptr)
    v = pde->getField();
  double vMid = 0;
  for (unsigned i = 0; i < v.size(); i++)
    vMid += v[i] / v.size();
  double sum = 0;
  for (unsigned int i = 0; i < v.size(); i++)
    sum += pow(v[i] - vMid, 2.);
  return sqrt(sum / v.size());
}
