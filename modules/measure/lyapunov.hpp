/*
   A module providing routines for calculating the lyapunov exponent spectrum
 */

#pragma once
#include "stepper/stepper.hpp"
#include <vector>

namespace measure {

class LyapunovExponents {
public:
  LyapunovExponents(Stepper *stepper, int n_exponents, int steps = 200,
                    double epsilon = 1e-6);
  ~LyapunovExponents();

  // make integration steps, reorthonormalize and update lyapunov exponents
  void step();
  // overload for multiple steps at once
  void steps(int steps);
  void integrate(double T);

  // the time-stepper for integration of the model
  Stepper *stepper;
  // the number of exponents to be calculated
  int n_exponents;
  // the number of integration steps to be performed each loop
  int integrationSteps;
  // the amplitude of the perturbation
  double epsilon;
  // vector of lyapunov exponents
  std::vector<double> exponents;

private:
  // counts total integration time
  double T = 0;
  // calculation variables
  std::vector<double> sum, norm;
  int dimension;
  // vector of lyapunov vectors, reference at position 0
  std::vector<std::vector<double>> trajectories;
  // orthonormalize basis vectors (Gram-Schmidt)
  void orthonormalize();
};

} // namespace measure