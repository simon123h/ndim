
#include "lyapunov.hpp"
#include "math/math.hpp"
#include "math/random.hpp"

using namespace measure;

// calculates the Lyapunov exponents for the time-dynamics of a model
LyapunovExponents::LyapunovExponents(Stepper *stepper, const int n_exponents,
                                     const int steps, const double epsilon)
    : stepper(stepper), n_exponents(n_exponents), integrationSteps(steps),
      epsilon(epsilon) {
  dimension = stepper->model->dimension;
  sum = std::vector<double>(n_exponents);
  norm = std::vector<double>(n_exponents);
  exponents = std::vector<double>(n_exponents);
  // initialize vector of random orthonormal lyapunov vectors
  // with reference at position 0
  for (int i = 0; i < n_exponents + 1; i++)
    trajectories.push_back(math::randoms<double>(dimension));
  orthonormalize();
}

LyapunovExponents::~LyapunovExponents() {}

// generate the new orthonormal set of vectors (GSR)
void LyapunovExponents::orthonormalize() {
  for (int i = 1; i < n_exponents + 1; i++) {
    // construct new vectors with GSR-coefficients (sum formula)
    for (int j = 1; j <= i - 1; j++) {
      double gsc = 0;
      for (int k = 0; k < dimension; k++)
        gsc += trajectories[i][k] * trajectories[j][k];
      for (int k = 0; k < dimension; k++)
        trajectories[i][k] -= gsc * trajectories[j][k];
    }
    // normalize vectors
    double tmp = 0;
    for (int k = 0; k < dimension; k++)
      tmp += trajectories[i][k] * trajectories[i][k];
    tmp = sqrt(tmp);
    norm[i - 1] = tmp;
    for (int k = 0; k < dimension; k++)
      trajectories[i][k] /= tmp;
  }
}

// make integration steps, reorthonormalize and update lyapunov exponents
void LyapunovExponents::step() {
  // load reference from model into trajectories, cause it might have changed
  trajectories[0] = stepper->model->getVars();
  // add reference to scaled lyapunov vectors
  for (int i = 1; i < n_exponents + 1; i++)
    for (int k = 0; k < dimension; k++)
      trajectories[i][k] = trajectories[0][k] + trajectories[i][k] * epsilon;
  // save current time
  double t = stepper->model->t;
  // integrate every perturbed vector including reference (last)
  for (int k = n_exponents; k >= 0; k--) {
    stepper->model->setVars(trajectories[k]);
    stepper->model->t = t;
    stepper->steps(integrationSteps);
    trajectories[k] = stepper->model->getVars();
  }
  // subtract reference from vectors and reorthonormalize
  for (int i = 1; i < n_exponents + 1; i++)
    for (int k = 0; k < dimension; k++)
      trajectories[i][k] -= trajectories[0][k];
  orthonormalize();
  // accumulate integration time
  T += stepper->model->t - t;
  // update lyapunov exponents
  for (int i = 0; i < n_exponents; i++) {
    sum[i] += log(norm[i] / epsilon);
    exponents[i] = sum[i] / T;
  }
}

// overload for multiple steps at once
void LyapunovExponents::steps(int steps) {
  for (int i = 0; i < steps; i++)
    step();
}
void LyapunovExponents::integrate(double T) {
  steps(std::round(T / stepper->dt));
}
