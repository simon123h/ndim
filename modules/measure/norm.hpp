#pragma once
#include "model/model.hpp"
#include "model/pde.hpp"

namespace measure {

// L2-Norm of variables
double l2norm(Model *model);

// roughness of variables
double roughness(Model *model);

} // namespace measure