#pragma once
#include "model/pde.hpp"
#include <algorithm>
#include <vector>

namespace measure {

// calculate the lateral drift of a field of arbitrary dimension (model->rank)
// according to formula from thesis
std::vector<double> drift(PDE *model);

} // namespace measure