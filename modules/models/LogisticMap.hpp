#pragma once
#include "model/model.hpp"
#include <iostream>

class LogisticMap : public Model {
private:
public:
  LogisticMap(double r = 1);
  ~LogisticMap();

  // governing equation
  std::vector<double> rhs(const std::vector<double> &vars, double t = 0);
  std::vector<std::vector<double>> jacobian(const std::vector<double> &vars,
                                            double t = 0);

  // parameter
  double r;
};

LogisticMap::LogisticMap(double r) : Model(1), r(r) {
  parameters = std::vector<double *>{&this->r};
  // this model is a discrete dynamical system
  is_time_discrete = true;
}

LogisticMap::~LogisticMap() {}

std::vector<double> LogisticMap::rhs(const std::vector<double> &vars,
                                     double t) {
  std::vector<double> result(dimension);
  result[0] = vars[0] * r * (1. - vars[0]) - vars[0];
  return result;
}

std::vector<std::vector<double>>
LogisticMap::jacobian(const std::vector<double> &vars, double t) {
  std::vector<std::vector<double>> result(1, std::vector<double>(1));
  result[0][0] = -2 * vars[0] * r + r - 1;
  return result;
}