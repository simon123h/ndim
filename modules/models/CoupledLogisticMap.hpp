#pragma once
#include "model/model.hpp"
#include <iostream>

class CoupledLogisticMap : public Model {
private:
public:
  CoupledLogisticMap(double r = 1, double d = 0.1);
  ~CoupledLogisticMap();

  // governing equation
  std::vector<double> rhs(const std::vector<double> &vars, double t = 0);

  // parameters
  double r, d;
};

CoupledLogisticMap::CoupledLogisticMap(double r, double d)
    : Model(2), r(r), d(d) {
  parameters = std::vector<double *>{&this->r, &this->d};
  // this model is a discrete dynamical system
  is_time_discrete = true;
}

CoupledLogisticMap::~CoupledLogisticMap() {}

std::vector<double> CoupledLogisticMap::rhs(const std::vector<double> &vars,
                                            double t) {
  std::vector<double> result(dimension);
  result[0] = vars[0] * r * (1. - vars[0]) - d * (vars[0] - vars[1]) - vars[0];
  result[1] = vars[1] * r * (1. - vars[1]) - d * (vars[1] - vars[0]) - vars[1];
  return result;
}
