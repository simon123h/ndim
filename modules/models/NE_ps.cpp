#include "NE_ps.hpp"
#include <fstream>

// constructor for 1d Nikolaevsiky equation
NE::NE(double L, int N, double r, double alpha, double beta)
    : NE(std::vector<double>{L}, std::vector<int>{N}, r, alpha, beta) {}
// constructor for n-dim. Nikolaevsiky equation
NE::NE(const std::vector<double> &size, const std::vector<int> &points,
       double r, double alpha, double beta)
    : PseudospectralPDE(size, points), r(r), alpha(alpha), beta(beta) {
  parameters = std::vector<double *>{&this->r, &this->alpha, &this->beta};
  // generate k-vector and squared k-vector
  k = fft->fftfreq(size);
  ksq = fft->fftfreqsq(size);
  // allocate temporary variables
  rtemp1 = fftw_alloc_real(realPoints);
  rtemp2 = fftw_alloc_real(realPoints);
  ctemp1 = reinterpret_cast<complex *>(fftw_alloc_complex(fourierPoints));
  // create own FFTW plans, more performant than using the FFT member
  plan_fft =
      fftw_plan_dft_r2c(rank, mesh->points.data(), rtemp2,
                        reinterpret_cast<fftw_complex *>(ctemp1), FFTW_MEASURE);
  plan_ifft = fftw_plan_dft_c2r(rank, mesh->points.data(),
                                reinterpret_cast<fftw_complex *>(ctemp1),
                                rtemp1, FFTW_MEASURE);
  fourierNorm = 1. / (realPoints);
}
// constructor for n-dim. Nikolaevsiky equation
NE::NE(const std::vector<double> &size, double r)
    : NE(size, guessGridpoints(size), r) {}
// constructor for NE with parameters as in Thesis
NE::NE(double r, double m, double gamma, const std::vector<int> &points)
    : NE(sizeFromParams(r, m, gamma),
         points.size() == 0 ? guessGridpoints(sizeFromParams(r, m, gamma))
                            : points,
         r) {
  this->m = m;
  this->gamma = gamma;
  old_m = m;
}

NE::~NE() {
  fftw_free(rtemp1);
  fftw_free(rtemp2);
  fftw_free(ctemp1);
  fftw_destroy_plan(plan_fft);
  fftw_destroy_plan(plan_ifft);
}

// linear part
std::vector<complex> NE::linear(const std::vector<complex> &vars, double t) {
  std::vector<complex> result(fourierPoints);
  for (int i = 0; i < fourierPoints; i++)
    result[i] = (ksq[i] * (r - pow(1. - ksq[i], 2)) - alpha) * vars[i];
  return result;
}

// nonlinear part
std::vector<complex> NE::nonlinear(const std::vector<complex> &vars, double t) {
  // set temporary cumulative vector to zero
  for (int i = 0; i < realPoints; i++)
    rtemp2[i] = 0;
  // loop over dimensions <d>
  for (int d = 0; d < rank; d++) {
    // spatial derivative in dimension <d>
    for (int i = 0; i < fourierPoints; i++)
      ctemp1[i] = math::I * k[d][i] * vars[i];
    // transform to real space
    fftw_execute(plan_ifft);
    // square derivative and add to sum for each dimension, also normalize
    for (int i = 0; i < realPoints; i++)
      rtemp2[i] += -0.5 * pow(rtemp1[i], 2.) * fourierNorm;
  }
  // add squared damping
  if (beta > 1e-10) {
    for (int i = 0; i < fourierPoints; i++)
      ctemp1[i] = vars[i];
    // transform to real space
    fftw_execute(plan_ifft);
    for (int i = 0; i < realPoints; i++)
      rtemp2[i] += beta * pow(rtemp1[i], 2.) * fourierNorm;
  }
  // transform back into fourier space and return
  fftw_execute(plan_fft);
  return std::vector<complex>(ctemp1, ctemp1 + fourierPoints);
}

// governing equation
std::vector<complex> NE::rhs(const std::vector<complex> &vars, double t) {
  // did the system size change? --> adjust mesh
  if (old_m != m)
    resize();
  // calculate and sum up linear and nonlinear parts
  std::vector<complex> lin = linear(vars);
  std::vector<complex> nl = nonlinear(vars);
  std::vector<complex> result(fourierPoints);
  for (int i = 0; i < fourierPoints; i++)
    result[i] = lin[i] + nl[i];
  return result;
}

// save the configuration to a file
void NE::save(std::string fname) {
  std::ofstream out(fname, std::ios::app);
  out << "r = " << r << "\n";
  out << "m = "
      << mesh->size[0] * sqrt(1 + sqrt(std::max(r, 0.))) / 2. / math::PI
      << "\n";
  if (rank > 1)
    out << "gamma = " << mesh->size[1] / mesh->size[0] << "\n";
  out << "alpha = " << alpha << "\n";
  out << "beta = " << beta << "\n";
  out.close();
  PDE::save(fname);
}

// find good number of discretization points for grid
std::vector<int> NE::guessGridpoints(const std::vector<double> &size) {
  std::vector<int> result(size.size());
  for (unsigned d = 0; d < size.size(); d++)
    result[d] = FFT::getGoodFFTWNumber(ceil(12 * size[d] / (2 * math::PI)));
  return result;
}

// generate the size from parameters m, r & gamma
std::vector<double> NE::sizeFromParams(double r, double m, double gamma) {
  double L = m * 2 * math::PI / sqrt(1 + sqrt(std::max(r, 0.)));
  if (fabs(gamma) < 1e-10)
    return std::vector<double>{L};
  else
    return std::vector<double>{L, L * gamma};
}
// resize NE1d & 2d after parameter change
void NE::resize() {
  std::vector<double> size = sizeFromParams(r, m, gamma);
  std::vector<int> points = mesh->points;
  delete mesh;
  mesh = new Mesh(size, points);
  k = fft->fftfreq(size);
  ksq = fft->fftfreqsq(size);
  old_m = m;
}