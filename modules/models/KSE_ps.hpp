#pragma once
#include "math/math.hpp"
#include "model/pde_ps.hpp"
#include <iostream>

/*
 * Pseudospectral implementation of the n-dimensional Kuramoto-Sivashinsky
 * equation, a nonlinear PDE
 */

class KSE : public PseudospectralPDE {
private:
  // temporary variables, allocated once for performance reasons
  complex *ctemp1;
  double *rtemp1, *rtemp2;
  fftw_plan plan_fft, plan_ifft;
  double fourierNorm;

public:
  // constructors for 1-dim. and n-dim. KS equation
  KSE(double L, int N, double alpha = 0, double beta = 0);
  KSE(const std::vector<double> &size, const std::vector<int> &points,
      double alpha = 0, double beta = 0);
  KSE(const std::vector<double> &size, double alpha);
  ~KSE();
  // the parameters
  double alpha, beta;
  // k-vector squared
  std::vector<double> ksq;
  // governing equation
  std::vector<complex> linear(const std::vector<complex> &vars, double t = 0);
  std::vector<complex> nonlinear(const std::vector<complex> &vars,
                                 double t = 0);
  std::vector<complex> rhs(const std::vector<complex> &vars, double t = 0);

  // save the configuration to a file
  virtual void save(std::string fname);

  // find good number of discretization points for grid
  static std::vector<int> guessGridpoints(const std::vector<double> &size);
};