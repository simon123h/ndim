#include "SHE_ps.hpp"
#include <fstream>

// constructor for 1d Swift-Hohenberg equation
SHE::SHE(double L, int N, double r, double kc, double v, double g)
    : SHE(std::vector<double>{L}, std::vector<int>{N}, r, kc, v, g) {}
// constructor for n-dim. Swift-Hohenberg equation
SHE::SHE(const std::vector<double> &sizes, const std::vector<int> &points,
         double r, double kc, double v, double g)
    : PseudospectralPDE(sizes, points), r(r), kc(kc), v(v), g(g) {
  parameters = std::vector<double *>{&this->r, &this->kc, &this->v, &this->g};
  // generate k-vector and squared k-vector
  k = fft->fftfreq(sizes);
  ksq = fft->fftfreqsq(sizes);
  // allocate temporary variables
  rtemp1 = fftw_alloc_real(realPoints);
  rtemp2 = fftw_alloc_real(realPoints);
  ctemp1 = reinterpret_cast<complex *>(fftw_alloc_complex(fourierPoints));
  // create own FFTW plans, more performant than using the FFT member
  plan_fft =
      fftw_plan_dft_r2c(rank, mesh->points.data(), rtemp2,
                        reinterpret_cast<fftw_complex *>(ctemp1), FFTW_MEASURE);
  plan_ifft = fftw_plan_dft_c2r(rank, mesh->points.data(),
                                reinterpret_cast<fftw_complex *>(ctemp1),
                                rtemp1, FFTW_MEASURE);
  fourierNorm = 1. / (realPoints);
}

SHE::~SHE() {
  fftw_free(rtemp1);
  fftw_free(rtemp2);
  fftw_free(ctemp1);
  fftw_destroy_plan(plan_fft);
  fftw_destroy_plan(plan_ifft);
}

// linear part
std::vector<complex> SHE::linear(const std::vector<complex> &vars, double t) {
  std::vector<complex> result(fourierPoints);
  for (int i = 0; i < fourierPoints; i++)
    result[i] = (r - pow((kc*kc - ksq[i]), 2.)) * vars[i];
  return result;
}

// nonlinear part
std::vector<complex> SHE::nonlinear(const std::vector<complex> &vars,
                                    double t) {
  // set temporary cumulative vector to zero
  for (int i = 0; i < realPoints; i++)
    rtemp2[i] = 0;
  // add squared and cubic terms
  for (int i = 0; i < fourierPoints; i++)
    ctemp1[i] = vars[i];
  // transform to real space
  fftw_execute(plan_ifft);
  for (int i = 0; i < realPoints; i++)
    rtemp2[i] += (v * pow(rtemp1[i], 2.) - g * pow(rtemp1[i], 3.)) * fourierNorm;
  // transform back into fourier space and return
  fftw_execute(plan_fft);
  return std::vector<complex>(ctemp1, ctemp1 + fourierPoints);
}

// governing equation
std::vector<complex> SHE::rhs(const std::vector<complex> &vars, double t) {
  // calculate and sum up linear and nonlinear parts
  std::vector<complex> lin = linear(vars);
  std::vector<complex> nl = nonlinear(vars);
  std::vector<complex> result(fourierPoints);
  for (int i = 0; i < fourierPoints; i++)
    result[i] = lin[i] + nl[i];
  return result;
}

// save the configuration to a file
void SHE::save(std::string fname) {
  std::ofstream out(fname, std::ios::app);
  out << "r = " << r << "\n";
  out << "kc = " << kc << "\n";
  out << "v = " << v << "\n";
  out << "g = " << g << "\n";
  out.close();
  PDE::save(fname);
}