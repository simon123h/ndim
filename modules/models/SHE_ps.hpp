#pragma once
#include "math/math.hpp"
#include "model/pde_ps.hpp"
#include <iostream>

/*
 * Pseudospectral implementation of the n-dimensional Swift-Hohenberg Equation
 * equation, a nonlinear PDE
 * \partial t h &= (r - (kc^2 + \Delta)^2)h + v * h^2 - g * h^3
 */

class SHE : public PseudospectralPDE {
private:
  // temporary variables, allocated once for performance reasons
  complex *ctemp1;
  double *rtemp1, *rtemp2;
  fftw_plan plan_fft, plan_ifft;
  double fourierNorm;

public:
  // constructors for 1-dim. and n-dim. KS equation
  SHE(double L, int N, double r = 0, double kc = 1, double v = 0, double g = 0);
  SHE(const std::vector<double> &size, const std::vector<int> &points,
      double r = 0, double kc = 1, double v = 0, double g = 0);
  ~SHE();
  // the parameters
  double r, kc, v, g;
  // k-vector squared
  std::vector<double> ksq;
  // governing equation
  std::vector<complex> linear(const std::vector<complex> &vars, double t = 0);
  std::vector<complex> nonlinear(const std::vector<complex> &vars,
                                 double t = 0);
  std::vector<complex> rhs(const std::vector<complex> &vars, double t = 0);

  // save the configuration to a file
  virtual void save(std::string fname);

  // find good number of discretization points for grid
  static std::vector<int> guessGridpoints(const std::vector<double> &size);
};