#pragma once
#include "model/pde_fd.hpp"
#include <iostream>

class NE1d : public FiniteDifferencesPDE {
private:
  /* data */
public:
  NE1d(double L, double r, int N);
  ~NE1d();
  // the parameter
  double r;
  // governing equation
  std::vector<double> rhs(const std::vector<double> &vars, double t = 0);
};

NE1d::NE1d(double L, double r, int N) : FiniteDifferencesPDE(L, N), r(r) {
  parameters = std::vector<double *>{&this->r};
}

NE1d::~NE1d() {}

// governing equation
std::vector<double> NE1d::rhs(const std::vector<double> &vars, double t) {
  std::vector<double> result = std::vector<double>(dimension);
  std::vector<double> d1 = ddx(vars, 0, 1);
  std::vector<double> d2 = ddx(vars, 0, 2);
  std::vector<double> d4 = ddx(vars, 0, 4);
  std::vector<double> d6 = ddx(vars, 0, 6);
  for (int i = 0; i < dimension; i++)
    result[i] = (1. - r) * d2[i] + 2 * d4[i] + d6[i] - 0.5 * d1[i] * d1[i];
  return result;
}
