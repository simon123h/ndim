#pragma once
#include "model/pde_fd.hpp"

class BladeCoating : public FiniteDifferencesPDE {
private:
public:
  BladeCoating(double L, int N, double alpha, double U, double G, double hLeft);
  ~BladeCoating();
  // the parameters
  double alpha, U, G, hLeft;
  // governing equation
  std::vector<double> rhs(const std::vector<double> &vars, double t = 0);
};

BladeCoating::BladeCoating(double L, int N, double alpha, double U, double G,
                     double hLeft)
    : FiniteDifferencesPDE(L, N), alpha(alpha), U(U), G(G), hLeft(hLeft) {
  parameters =
      std::vector<double *>{&this->alpha, &this->U, &this->G, &this->hLeft};
}

BladeCoating::~BladeCoating() {}

// governing equation
std::vector<double> BladeCoating::rhs(const std::vector<double> &vars, double t) {
  std::vector<double> result(dimension);
  double dx = mesh->x[0][1] - mesh->x[0][0];

  // boundary conditions
  std::vector<double> HH(dimension + 4);
  for (int i = 0; i < dimension; i++)
    HH[i + 2] = vars[i];
  // BC left: meniscus
  HH[1] = hLeft;
  HH[0] = HH[2] + alpha * 2 * dx; // 1st derivative = -alpha
  // BC right: flat film
  HH[dimension + 2] = HH[dimension + 1]; // 1st derivative zero
  HH[dimension + 3] = HH[dimension + 1]; // 2nd derivative zero

  double d1, d2, d3, d4, fh, fhh, term1, term2, term3;
  for (int i = 0; i < dimension; i++) {
    // calculate derivatives
    d1 = (HH[i + 2 + 1] - HH[i + 2 - 1]) / 2 / dx;
    d2 = (HH[i + 2 + 1] - 2 * HH[i + 2] + HH[i + 2 - 1]) / pow(dx, 2.);
    d3 = (HH[i + 2 + 2] - 2 * HH[i + 2 + 1] + 2 * HH[i + 2 - 1] -
          HH[i + 2 - 2]) /
         2 / pow(dx, 3.);
    d4 = (HH[i + 2 + 2] - 4 * HH[i + 2 + 1] + 6 * HH[i + 2] -
          4 * HH[i + 2 - 1] + HH[i + 2 - 2]) /
         pow(dx, 4.);
    // define derjaguin pressure
    fh = -3 * (1 - 2 / pow(vars[i], 3)) / pow(vars[i], 4);
    fhh = 6 * (2 - 7 / pow(vars[i], 3)) / pow(vars[i], 5);
    term1 = pow(vars[i], 2) * d1 * (-d3 + fh * d1);
    term2 = pow(vars[i], 3) * (-d4 + fhh * pow(d1, 2) + fh * d2) / 3;
    term3 =
        G * pow(vars[i], 2) * d1 * (d1 + alpha) + G * pow(vars[i], 3) * d2 / 3;
    // sum up to result
    result[i] = term1 + term2 + term3 - U * d1;
  }
  return result;
}
