#pragma once
#include "math/math.hpp"
#include "model/pde_ps.hpp"
#include <iostream>

// the thin film equation as given in GTLT2014prl

class Drop : public PseudospectralPDE {
private:
public:
  Drop(double L, int N);
  Drop(const std::vector<double> &size, const std::vector<int> &points);
  ~Drop();
  // the parameters
  double eta = 1. / 3.;
  double gamma = 1.;
  double U = 0.;
  // k-vector squared
  std::vector<double> ksq;
  // governing equation
  std::vector<complex> rhs(const std::vector<complex> &vars, double t = 0);
};

// constructors
Drop::Drop(double L, int N)
    : Drop(std::vector<double>{L}, std::vector<int>{N}) {}
Drop::Drop(const std::vector<double> &size, const std::vector<int> &points)
    : PseudospectralPDE(size, points) {
  ksq = fft->fftfreqsq(size);
}
// destructor
Drop::~Drop() {}

// governing equation
std::vector<complex> Drop::rhs(const std::vector<complex> &vars, double t) {
  std::vector<double> result(realPoints, 0.);
  // calculate real field by FFT
  std::vector<complex> ctemp1 = vars;
  std::vector<double> h = fft->ifft(ctemp1);
  // calculate Laplace bit
  for (int i = 0; i < fourierPoints; i++)
    ctemp1[i] = -ksq[i] * vars[i];
  std::vector<double> deltah = fft->ifft(ctemp1);
  // calculate variation of free energy
  std::vector<double> dFdh(realPoints, 0.);
  for (int i = 0; i < realPoints; i++)
    dFdh[i] = gamma * deltah[i] - std::pow(h[i], -3.) + std::pow(h[i], -6.);
  // calculate gradient of dFdh, multiply mobility and gradient again
  std::vector<complex> ftdFdh = fft->fft(dFdh);
  for (int d = 0; d < rank; d++) {
    for (int i = 0; i < fourierPoints; i++)
      ctemp1[i] = math::I * k[d][i] * ftdFdh[i];
    std::vector<double> grad = fft->ifft(ctemp1);
    // multiply mobility
    for (int i = 0; i < realPoints; i++)
      grad[i] *= std::pow(h[i], 3.) / 3. / eta;
    // gradient again
    ctemp1 = fft->fft(grad);
    for (int i = 0; i < fourierPoints; i++)
      ctemp1[i] *= math::I * k[d][i];
    grad = fft->ifft(ctemp1);
    // add to result
    for (int i = 0; i < realPoints; i++)
      result[i] += grad[i];
  }
  return fft->fft(result);
}
