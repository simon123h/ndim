#pragma once
#include "model/ode.hpp"
#include <iostream>

class LotkaVolterra : public ODE {
private:
public:
  LotkaVolterra(double a = 0.1, double b = 0.02, double c = 0.4,
                double d = 0.02);
  ~LotkaVolterra();

  // governing equation
  std::vector<double> rhs(const std::vector<double> &vars, double t = 0);

  // parameters
  double a, b, c, d;
};

LotkaVolterra::LotkaVolterra(double a, double b, double c, double d)
    : ODE(2), a(a), b(b), c(c), d(d) {
  parameters = std::vector<double *>{&this->a, &this->b, &this->c, &this->d};
}

LotkaVolterra::~LotkaVolterra() {}

std::vector<double> LotkaVolterra::rhs(const std::vector<double> &vars,
                                       double t) {
  std::vector<double> result(dimension);
  double x = vars[0];
  double y = vars[1];
  result[0] = x * (a - b * y);
  result[1] = y * (d * x - c);
  return result;
}