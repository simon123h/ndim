#pragma once
#include "math/math.hpp"
#include "model/pde_ps.hpp"
#include <algorithm>
#include <iostream>

/*
 * Pseudospectral implementation of the n-dimensional Nikolaevsiky equation
 * with KS-nonlinearity, a nonlinear PDE
 */

class NE : public PseudospectralPDE {
private:
  // temporary variables, allocated once for performance reasons
  complex *ctemp1;
  double *rtemp1, *rtemp2;
  fftw_plan plan_fft, plan_ifft;
  double fourierNorm;
  // storage for old m parameter, to notice change in system size
  double old_m = -1;

public:
  // constructors for 1-dim. and n-dim. Nikolaevsiky equation
  NE(double L, int N, double r = 0.5, double alpha = 0, double beta = 0);
  NE(const std::vector<double> &size, const std::vector<int> &points,
     double r = 0.5, double alpha = 0, double beta = 0);
  NE(const std::vector<double> &size, double r = 0.5);
  // constructor for NE with parameters as in Thesis
  NE(double r, double m, double gamma = 0,
     const std::vector<int> &points = std::vector<int>{});
  ~NE();
  // the parameters
  double m = -1, gamma, r, alpha, beta;
  // k-vector squared
  std::vector<double> ksq;
  // governing equation
  std::vector<complex> linear(const std::vector<complex> &vars, double t = 0);
  std::vector<complex> nonlinear(const std::vector<complex> &vars,
                                 double t = 0);
  std::vector<complex> rhs(const std::vector<complex> &vars, double t = 0);

  // save the configuration to a file
  virtual void save(std::string fname);

  // find good number of discretization points for grid
  static std::vector<int> guessGridpoints(const std::vector<double> &size);
  // generate the size from parameters m, r & gamma
  std::vector<double> sizeFromParams(double r, double m, double gamma = 0);
  // resize NE1d & 2d according to the following parameters
  void resize();
};

class NE_comoving : public NE {
private:
public:
  NE_comoving(double r, double m, double gamma = 0,
              const std::vector<int> &points = std::vector<int>{})
      : NE(r, m, gamma, points) {
    fixedModes = std::vector<int>(rank);
    drift = std::vector<double>(rank);
  }
  ~NE_comoving() {}
  std::vector<int> fixedModes;
  std::vector<double> drift;

  std::vector<complex> nonlinear(const std::vector<complex> &vars,
                                 double t = 0) {
    std::vector<complex> result = NE::nonlinear(vars, t);
    for (int i = 0; i < fourierPoints; i++)
      for (int d = 0; d < rank; d++)
        result[i] -= math::I * k[d][i] * drift[d] * vars[i];
    return result;
  }

  // convert variables from Fourier domain to degrees-of-freedom domain
  std::vector<double> four2vars(const std::vector<complex> &four) {
    std::vector<double> result(dimension);
    if (rank == 1) {
      int pos = 0;
      int N = mesh->points[0];
      for (int i = 0; i < N / 2 + 1; i++) {
        if (std::find(ignoreModesReal.begin(), ignoreModesReal.end(), i) !=
            ignoreModesReal.end())
          continue;
        result[pos++] = four[i].real();
        if (i == 0 || i == N / 2) // exclude zero- and Nyquist modes
          continue;
        if (std::find(ignoreModesImag.begin(), ignoreModesImag.end(), i) !=
            ignoreModesImag.end())
          continue;
        if (std::find(fixedModes.begin(), fixedModes.end(), i) !=
            fixedModes.end()) {
          // TODO: include drift speed here
          continue;
        }
        result[pos++] = four[i].imag();
      }
    } else if (rank == 2) {
      int pos = 0;
      int Nx = mesh->points[0];
      int Ny = mesh->points[1];
      for (int i = 0; i < Nx; i++) {
        for (int j = 0; j < Ny / 2 + 1; j++) {
          if (i > Nx / 2 &&
              (j == 0 || j == Ny / 2)) // exclude complex conjugated modes
            continue;
          int ind = i * (Ny / 2 + 1) + j;
          if (std::find(ignoreModesReal.begin(), ignoreModesReal.end(), ind) !=
              ignoreModesReal.end())
            continue;
          result[pos++] = four[ind].real();
          if ((i == 0 || i == Nx / 2) &&
              (j == 0 || j == Ny / 2)) // exclude zero- and Nyquist modes
            continue;
          if (std::find(ignoreModesImag.begin(), ignoreModesImag.end(), ind) !=
              ignoreModesImag.end())
            continue;
          if (std::find(fixedModes.begin(), fixedModes.end(), i) !=
              fixedModes.end()) {
            // TODO: include drift speed here
            continue;
          }
          result[pos++] = four[ind].imag();
        }
      }
    } else {
      for (unsigned i = 0; i < four.size(); i++) {
        result[2 * i] = four[i].real();
        result[2 * i + 1] = four[i].imag();
      }
    }
    return result;
  }

  // convert variables from degrees-of-freedom domain to Fourier domain
  std::vector<complex> vars2four(const std::vector<double> &vars) {
    std::vector<complex> result(fft->fourierPoints);
    if (rank == 1) {
      int pos = 0;
      int N = mesh->points[0];
      for (int i = 0; i < N / 2 + 1; i++) {
        if (std::find(ignoreModesReal.begin(), ignoreModesReal.end(), i) !=
            ignoreModesReal.end())
          continue;
        result[i] = complex(vars[pos++], 0);
        if (i == 0 || i == N / 2) // exclude zero- and Nyquist modes
          continue;
        if (std::find(ignoreModesImag.begin(), ignoreModesImag.end(), i) !=
            ignoreModesImag.end())
          continue;
        if (std::find(fixedModes.begin(), fixedModes.end(), i) !=
            fixedModes.end()) {
          // TODO: include drift speed here
          continue;
        }
        result[i] += complex(0, vars[pos++]);
      }
    } else if (rank == 2) {
      int pos = 0;
      int Nx = mesh->points[0];
      int Ny = mesh->points[1];
      for (int i = 0; i < Nx; i++) {
        for (int j = 0; j < Ny / 2 + 1; j++) {
          if (i > Nx / 2 &&
              (j == 0 || j == Ny / 2)) // exclude complex conjugated modes
            continue;
          int ind = i * (Ny / 2 + 1) + j;
          if (std::find(ignoreModesReal.begin(), ignoreModesReal.end(), ind) !=
              ignoreModesReal.end())
            continue;
          result[ind] = vars[pos++];
          if ((i == 0 || i == Nx / 2) &&
              (j == 0 || j == Ny / 2)) // exclude zero- and Nyquist modes
            continue;
          if (std::find(ignoreModesImag.begin(), ignoreModesImag.end(), ind) !=
              ignoreModesImag.end())
            continue;
          if (std::find(fixedModes.begin(), fixedModes.end(), i) !=
              fixedModes.end()) {
            // TODO: include drift speed here
            continue;
          }
          result[ind] += vars[pos++] * math::I;
        }
      }
      // restore complex conjugated values
      for (int i = Nx / 2; i < Nx; i++) {
        result[i * (Ny / 2 + 1) + 0] =
            std::conj(result[(Nx - i) * (Ny / 2 + 1) + 0]);
        result[i * (Ny / 2 + 1) + Ny / 2] =
            std::conj(result[(Nx - i) * (Ny / 2 + 1) + Ny / 2]);
      }
    }
    return result;
  }
};