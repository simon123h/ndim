# TODO

## refactoring

## minor tweaks
- remove parameters vector: unnecessary
- use unsigned instead of int, when unsigned is meant

## major tops
- test eigenvalue spectrum of jacobian
- implement bifurcation detection from eigenvalues

## Future
- add general plot.plt scripts
- standardize tests, Test class or something?
- find a way to do branch switching
- support vector-valued equations