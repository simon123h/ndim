###
###  GLOBAL MAKEFILE
###  included by every specific makefile
###


### Adjust only the following variables for each problem:
# # buildable main targets: matches against $(TARGETS).cpp files
# TARGETS := my_program
# # source files
# SRCS := my_source1.cpp my_source2.cpp
# # root of the project
# PROJECTROOT = ../..


### Compiler and linker settings
ifeq "$(shell which icpc >/dev/null; echo $$?)" "0"
	# Intel compiler
	CXX := icpc
	CXXFLAGS := -std=c++17 -O3 -fopenmp -restrict -g -Wall -wd3175 -I $(PROJECTROOT)/external/eigen
	# CXXFLAGS := -std=c++17 -O3 -xHost -fopenmp -restrict -g -qopt-report=3
	LD := icpc
	LDFLAGS := -static -O3 -lfftw3 -lm -lstdc++fs -Wl,--whole-archive -lpthread -Wl,--no-whole-archive -fopenmp -I $(PROJECTROOT)/external/eigen
else
	# GNU compiler
	CXX := g++
	CXXFLAGS := -std=c++17 -O3 -g -Wall -Wno-unused-result -fopenmp -I $(PROJECTROOT)/external/eigen
	LD := g++
	LDFLAGS := -lfftw3 -lm -lstdc++fs -O3 -Wl,--whole-archive -lpthread -Wl,--no-whole-archive -fopenmp -I $(PROJECTROOT)/external/eigen
endif
ifeq "$(DEBUG)" "true"
	# GNU compiler with debugging flags enabled
	CXX := g++
	CXXFLAGS = -std=c++17 -g -Wall -fopenmp -DPARANOID -DRANGE_CHECKING
	LD := g++
	LDFLAGS := -lm -fopenmp
endif

### Build workflow: makefile magic begins here!
# intermediate directory for generated object files
OBJDIR := .objs
# intermediate directory for generated dependency files
DEPDIR := .objs/.deps
# modules path
MODPATH = $(PROJECTROOT)/modules

# flags required for dependency generation; passed to compilers
DEPFLAGS = -MT $@ -MD -MP -MF $(DEPDIR)/$*.d

# compile command for C++ source files
COMPILE = $(CXX) $(DEPFLAGS) $(CXXFLAGS) -c -o $@ -I "$(MODPATH)"
# precompile step
PRECOMPILE = @echo "Compiling $(subst ../,,$<)";
# postcompile step
POSTCOMPILE =

# object files, auto generated from source files
OBJS := $(patsubst %,$(OBJDIR)/%.o,$(basename $(SRCS)))
# dependency files, auto generated from source files
DEPS := $(patsubst %,$(DEPDIR)/%.d,$(basename $(SRCS)))
DEPS += $(patsubst %,$(DEPDIR)/%.d,$(basename $(TARGETS)))

# create the output subdirectories
$(shell mkdir -p $(dir $(OBJS)) $(dir $(DEPS)) >/dev/null)
$(shell mkdir -p $(dir $(patsubst %,$(OBJDIR)/%,$(TARGETS))) >/dev/null)

# link compiled files for every target in $(TARGETS)
$(TARGETS): %: $(OBJDIR)/%.o $(OBJS)
	@echo "Linking $@"
	@$(LD) -o $@ $^ $(LDFLAGS)
	@echo "Done building: $@"
ifeq "$(DEBUG)" "true"
	@echo "WARNING: Debug mode on - code not optimised!"
endif

# compile C++ source files
$(OBJDIR)/%.o: %.cpp
$(OBJDIR)/%.o: %.cpp $(DEPDIR)/%.d
	$(PRECOMPILE)
	@$(COMPILE) $<
	$(POSTCOMPILE)

# rules for module source files
$(OBJDIR)/%.o: $(MODPATH)/%.cpp
$(OBJDIR)/%.o: $(MODPATH)/%.cpp $(DEPDIR)/%.d
	$(PRECOMPILE)
	@$(COMPILE) $<
	$(POSTCOMPILE)

# include generated dependencies
-include $(DEPS)
# don't delete dependency files
.PRECIOUS = $(DEPDIR)/%.d
$(DEPDIR)/%.d: ;

# first target in list $(TARGETS) is referenced as MAINTARGET
MAINTARGET := $(firstword $(TARGETS))


### Useful targets ("phony targets"): all callable via 'make <target>'

# build all targets with 'make all' or simply 'make'
.PHONY: all
.DEFAULT_GOAL := all
all: $(TARGETS)

# remove temporary compilation files with 'make clean'
.PHONY: clean
clean:
	$(RM) -r $(OBJDIR) $(DEPDIR) $(TARGETS)
.PHONY: new
new: clean
	@make all -j --no-print-directory

# make and run executables with 'make run'
.PHONY: run
run: $(MAINTARGET)
	$(patsubst %,./%;,$(MAINTARGET))

# compile with debugging enabled with 'make debug'
.PHONY: debug
debug:
	@make --eval=DEBUG=true --no-print-directory

# build debug and enter the GNU debugger with 'make gdb'
.PHONY: gdb
gdb: clean debug
	@make --eval=DEBUG=true --no-print-directory
	gdb $(MAINTARGET)

# echo the full build command
.PHONY: command
command:
	@echo "Compilation command:\n$(COMPILE)\n"
	@echo "Linking command:\n$(LD) $(LDFLAGS)"

# remove and recreate output folder with 'make out'
.PHONY: out
out:
	rm -rf out
	mkdir out
