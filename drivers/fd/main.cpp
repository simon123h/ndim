#include "math/math.hpp"
#include "math/random.hpp"
#include "measure/drift.hpp"
#include "measure/lyapunov.hpp"
#include "measure/norm.hpp"
#include "models/NE1d_fd.hpp"
#include "models/NE_ps.hpp"
#include "output/output.hpp"
#include "output/timer.hpp"
#include "stepper/etdrk4.hpp"
#include <iostream>

int main(int argc, char const *argv[]) {

  // parameters
  double r = 0;
  double m = 3;
  double gamma = 0;
  double T_transient = 10 / r;
  int plotEvery = 20;

  // create model and stepper
  NE *model = new NE(r, m, gamma, std::vector<int>{128});
  NE1d *fdm = new NE1d(model->mesh->size[0], r, model->mesh->points[0]);
  Stepper *stepper = new ETDRK4();
  stepper->setModel(model);
  stepper->setTimestep(0.002 / r);
  measure::LyapunovExponents *lyap = new measure::LyapunovExponents(stepper, 1);

  // clear output folder
  output::clearOut();
  output::mkdir("out/dat");

  // save model and parameters
  model->save("out/model.ini");

  // set initial values
  model->setField(math::randoms(model->realPoints, 1e-3));
  // model->setField(output::load("field.dat"));
  // integrate transient
  Timer timer = Timer("Integrating transient:", 4);
  while (model->t < T_transient) {
    timer.show(model->t / T_transient);
    stepper->steps(plotEvery);
    model->resetZeroMode();
  }

  std::vector<double> in(model->realPoints);
  for (int i = 0; i < model->realPoints; i++) {
    in[i] = sin(model->mesh->x[0][i]);
  }
  model->setField(in);

  output::save("out/m1r.dat", model->mesh->x, model->getField());
  output::save("out/m1d.dat", model->mesh->x,
               model->getSpatialDerivatives()[0]);

  fdm->setField(model->getField());
  output::save("out/m2r.dat", model->mesh->x, fdm->getField());
  output::save("out/m2d.dat", model->mesh->x, fdm->ddx(fdm->getField(), 0, 5));
  // output::save("out/m2d.dat", model->mesh->x,
  // fdm->getSpatialDerivatives()[0]);

  /*
   * Measure quantities
   */
  std::string fname = "out/quantities.dat";
  output::save(fname, "L2-norm: ");
  output::save(fname, measure::l2norm(model));
  output::save(fname, "Roughness: ");
  output::save(fname, measure::roughness(model));
  output::save(fname, "Drift: ");
  output::save(fname, measure::drift(model), true);
  output::save("field.dat", model->getField());
  system("cat out/quantities.dat");

  /*
   * Plot
   */
  if (model->rank == 1) {
    system("gnuplot spacetime.plt");
    system("gnuplot plot1d.plt > /dev/null 2>&1 &");
  }
  if (model->rank == 2) {
    system("gnuplot plot2d.plt > /dev/null 2>&1 &");
  }

  delete model;
  delete stepper;
  delete lyap;
  return 0;
}
