#include "generic.hpp"
#include "models/NE_ps.hpp"
#include "stepper/etdrk4.hpp"

int main(int argc, char const *argv[]) {

  // parameters
  double r = 0.5;
  double m = 3;
  double gamma = 1 / sqrt(3.);
  double T_transient = 10 / r;
  double T_lyapunov = 10000 / r;
  int plotEvery = 20;

  // create model and stepper
  NE *model = new NE(r, m, gamma);
  Stepper *stepper = new ETDRK4();
  stepper->setModel(model);
  stepper->setTimestep(0.002 / r);
  auto *lyap = new measure::LyapunovExponents(stepper, 15);

  // clear output folder
  output::clearOut();
  output::mkdir("out/dat");

  // save model and parameters
  model->save("out/model.ini");

  // set initial values
  model->setField(math::randoms(model->realPoints, 1e-3));
  // model->setField(output::load("field.dat"));

  // integrate transient
  Timer timer = Timer("Integrating transient:", 4);
  while (model->t < T_transient) {
    timer.show(model->t / T_transient);
    stepper->steps(plotEvery);
    model->resetZeroMode();
  }

  /*
   * Calculate lyapunov exponents
   */
  model->t = 0;
  timer = Timer("Calculating Lyapunov exponents:", 5);
  while (model->t < T_lyapunov) {
    lyap->step();
    timer.show(model->t / T_lyapunov);
    std::cout << "First LE: " << lyap->exponents[0] << std::endl;
    output::truncate("out/exponents.dat");
    output::save("out/exponents.dat", lyap->exponents);
    system("gnuplot lyapunov.plt");
  }

  delete model;
  delete stepper;
  delete lyap;
  return 0;
}
