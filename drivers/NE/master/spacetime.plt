set terminal pngcairo

# load palette
load "../../tools/palettes/moreland.pal"

set out "spacetime.png"
plot "out/spacetime.dat" matrix with image notitle

