#include "generic.hpp"
#include "models/NE_ps.hpp"
#include "stepper/etdrk4.hpp"
#include <fstream>

int main(int argc, char const *argv[]) {

  // system parameter ranges
  double min_r = 0.5;
  // double min_r = 0.5;
  double max_r = 1;
  double min_m = 1;
  double max_m = 10;
  double min_ratio = 0.005;
  // double min_ratio = 1;
  double max_ratio = 1;
  int steps_r = 1;
  int steps_m = 400;
  int steps_ratio = 200;
  // integration time
  double T_transient = 5000;
  double T_lyapunov = 10000;
  // number of lyapunov exponents to calculate
  int n_exponents = 4;
  // number of jobs
  int n_jobs = steps_r * steps_m * steps_ratio / 20;

  // try to get reduced parameter range from condor jobID
  int startIndex = 0;
  int endIndex = steps_r * steps_m * steps_ratio;
  if (argc > 1) {
    int jobSize = (steps_r * steps_m * steps_ratio) / n_jobs;
    startIndex = jobSize * atoi(argv[1]);
    endIndex = jobSize * (atoi(argv[1]) + 1);
  }

  // set up progressbar
  Timer timer = Timer("", 3);

  int index = 0;
  std::ofstream out("out/scan.dat", std::ios::app);
  for (double r : math::linspace(min_r, max_r, steps_r)) {
    for (double m : math::linspace(min_m, max_m, steps_m)) {
      for (double ratio : math::linspace(min_ratio, max_ratio, steps_ratio)) {
        if (index >= startIndex && index < endIndex) {
          math::randomSeed = index;
          // equation and stepper with new parameters
          NE *model = new NE(r, m, ratio);
          Stepper *stepper = new ETDRK4();
          stepper->setModel(model);
          stepper->setTimestep(0.002 / r);
          auto *lyap = new measure::LyapunovExponents(stepper, n_exponents);

          // set initial values
          model->setField(math::randoms(model->realPoints, 1e-3));

          // integrate transient
          while (model->t < T_transient / r) {
            stepper->step();
            model->resetZeroMode();
          }

          // calculate Lyapunov exponents
          model->t = 0;
          while (model->t < T_lyapunov) {
            lyap->step();
            model->resetZeroMode();
          }

          // calculate the drift speed
          std::vector<double> drift = measure::drift(model);

          // calculate median of absolute drift speed
          int avgSteps = 100;
          int avgSubsteps = 50;
          std::vector<double> driftsequence(avgSteps);
          for (int s = 0; s < avgSteps; s++) {
            stepper->steps(avgSubsteps);
            driftsequence[s] = math::norm(measure::drift(model));
          }
          std::sort(driftsequence.begin(), driftsequence.end());
          double driftMedian = driftsequence[avgSteps / 2];

          // calculate roughness
          double rghn = measure::roughness(model);

          // output
          output::save(output::format("out/dat/%08dr.dat", index),
                       model->mesh->x, model->getField());
          out << r << ' ' << m << ' ' << ratio << ' ';
          for (int i = 0; i < n_exponents; i++)
            out << lyap->exponents[i] << ' ';
          out << drift[0] << ' ' << drift[1] << ' ';
          out << driftMedian << ' ' << rghn << ' ';
          out << std::endl;

          // print progress
          timer.show((index - startIndex) / (double)(endIndex - startIndex));

          // destructors
          delete model;
          delete stepper;
          delete lyap;
        }
      }
    }
  }
  return 0;
}
