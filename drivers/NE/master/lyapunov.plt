#!/usr/bin/gnuplot

# set terminal pdf
# set out 'out/exponents.pdf'
set terminal pngcairo size 840,480 enhanced font 'Verdana,10'
set out "out/exponents.png"

set ylabel "Lyapunov spectrum"
set xlabel "n"
set xrange[*:*]
set yrange[*:*]
set autoscale xfix
set autoscale yfix
set grid

plot 'out/exponents.dat' using ($0):1 notitle with linespoints
