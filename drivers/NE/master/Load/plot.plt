#!/usr/bin/gnuplot

###
### plot 2D data (real & spectral)
###

## general setup (for both sides of the plot)
# nice looking & sized png's
set terminal pngcairo enhanced font 'Verdana,10'

# flat view on heatmap
set view map

# plot aspect is original aspect ratio
set size ratio -1

# delete raw data files after plotting?
deleteData = 0

# ranges dont stick to even numbers
set autoscale xfix
set autoscale yfix
set autoscale cbfix

# tics layout
set format cb "%g"
set format x '%.0P π'
set format y '%.0P π'
set xlabel 'x'
set ylabel 'y'
set cblabel 'h'

# load parameters
# load "out/info/parameters.plt"
# rtitle = sprintf('r = %g,	', r)

# load palette
load "../../../../src/tools/palettes/moreland.pal"
!mkdir -p out/img

# plot a single file or all?
if(exists("i")){
	start = i
	stop = i
	plotAll = 0
}else{
	start = 1
	stop = int("0".system("ls out/dat/*.dat | tail -n1 | cut -c9-13"))
	plotAll = 1
}

# maximum amplitude
amp = 0.001

# loop over all images if start!=stop
do for [i=start:stop]{
	stats 'out/dat/'.sprintf('%05d', i).'r.dat' using 3 prefix "v" nooutput
	if(v_max > amp){amp = v_max}
	if(-v_min > amp){amp = -v_min}
}
## real space plot
set xrange[*:*]
set yrange[*:*]
set cbrange[-0.95*amp:0.95*amp]
set xtics pi
set ytics pi
do for [i=start:stop]{
	id = sprintf('%05d', i)
	set out 'out/img/'.id.'.png'
	plot 'out/dat/'.id.'r.dat' using 1:2:3 with image notitle
}

! avconv -r 25 -i 'out/img/%05d.png' -y out.mp4
