#include "generic.hpp"
#include "models/NE_ps.hpp"
#include "output/plotter.hpp"
#include "stepper/etdrk4.hpp"
#include <fstream>

int main(int argc, char const *argv[]) {

  int Nx, Ny, plotEvery;
  double mx, r, T, T_transient;
  std::string dummy;
  if (!output::path_exists("parameters.txt")) {
    std::cout << "parameters.txt not found!" << std::endl;
    return 1;
  }
  std::ifstream pfile("parameters.txt");
  while (pfile >> dummy) {
    if (dummy == "Nx") {
      pfile >> dummy >> Nx;
      std::cout << "Nx = " << Nx << std::endl;
    }
    if (dummy == "Ny") {
      pfile >> dummy >> Ny;
      std::cout << "Ny = " << Ny << std::endl;
    }
    if (dummy == "mx") {
      pfile >> dummy >> mx;
      std::cout << "mx = " << mx << std::endl;
    }
    if (dummy == "r") {
      pfile >> dummy >> r;
      std::cout << "r = " << r << std::endl;
    }
    if (dummy == "T") {
      pfile >> dummy >> T;
      std::cout << "T = " << T << std::endl;
    }
    if (dummy == "plotEvery") {
      pfile >> dummy >> plotEvery;
      std::cout << "plotEvery = " << plotEvery << std::endl;
    }
    if (dummy == "T_transient") {
      pfile >> dummy >> T_transient;
      std::cout << "T_transient = " << T_transient << std::endl;
    }
  }
  pfile.close();

  // parameters
  // double r = 0.5;
  double m = mx;
  double gamma = 1.;
  // double T_transient = 10 / r;
  double T_integrate = T;
  // int plotEvery = 20;
  double my = gamma * m;
  const double Lx = mx * 2 * math::PI / sqrt(1 + sqrt(r));
  const double Ly = my * 2 * math::PI / sqrt(1 + sqrt(r));

  // create model and stepper
  NE *model = new NE(std::vector<double>{Lx, Ly}, std::vector<int>{Nx, Ny}, r);
  Stepper *stepper = new ETDRK4();
  stepper->setModel(model);
  stepper->setTimestep(0.002 / r);

  // clear output folder
  output::clearOut();
  output::mkdir("out/dat");

  // save model and parameters
  model->save("out/model.ini");

  // set initial values
  if (output::path_exists("real.dat"))
    model->setField(output::loadReal("real.dat", 3, 2));
  else if (output::path_exists("fourier.dat")) {
    std::vector<complex> init(model->fourierPoints);
    std::vector<double> re = output::load("fourier.dat", 0);
    std::vector<double> im = output::load("fourier.dat", 1);
    for (unsigned i = 0; i < init.size(); i++)
      init[i] = complex(re[i], im[i]);
    model->setFourier(init);
  } else
    model->setField(math::randoms<double>(Nx * Ny, 1e-5));

  // model->setField(math::randoms(model->realPoints, 1e-3));
  // model->setField(output::load("field.dat"));
  // integrate transient
  Timer timer = Timer("Integrating transient:", 4);
  while (model->t < T_transient) {
    timer.show(model->t / T_transient);
    stepper->steps(plotEvery);
    model->resetZeroMode();
  }

  std::vector<int> ff = model->mesh->points;
  for (unsigned i = 0; i < ff.size(); i++)
    ff[i] = 96;
  Mesh *finer = new Mesh(model->mesh->size, ff);

  /*
   * Time integration
   */
  model->t = 0;
  timer = Timer("Integrating dynamics:", 5);
  // int id = 0;
  while (model->t < T_integrate) {
    // make time steps
    stepper->steps(plotEvery);
    model->resetZeroMode();
    // print progress
    int id = timer.show(model->t / T_integrate);
    // id++;
    std::cout << "Step " << id << std::endl;
    // save field
    // output::save(output::format("out/dat/%05dr.dat", id), model->mesh->x,
    //              model->getField());
    output::save(output::format("out/dat/%05dr.dat", id), finer->x,
                 model->fft->interpolate(model->getField(), model->mesh->points,
                                         finer->points));
    output::saveFourier(output::format("out/dat/%05df.dat", id), model->k,
                        model->getFourier(), model->mesh->points);
    output::save("out/roughness.dat", measure::roughness(model));
  }

  /*
   * Measure quantities
   */
  system("rm -f fourier_out.dat");
  output::save("fourier_out.dat", math::real(model->getFourier()),
               math::imag(model->getFourier()));

  /*
   * Plot
   */
  system("gnuplot plot.plt > /dev/null 2>&1");

  delete model;
  delete stepper;
  return 0;
}
