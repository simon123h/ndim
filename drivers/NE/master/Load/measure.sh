#!/bin/bash

make;

exedir=`pwd`
datdir="/local0/s_hart20/Masterarbeit/dat/Simulation/DPG"

cd $datdir
# list=$(ls)
list="1c 2b 3a 4a"
# list="3a"
echo $list
for i in $list; do
    cd $i
    echo \\n!!! $i !!!
    cp $exedir/main .
    cp $exedir/plot.plt .
    (./main && echo "Done with $i!"; rm main) &
    cd ..
done;