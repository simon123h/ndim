#include "generic.hpp"
#include "models/NE_ps.hpp"
#include "stepper/etdrk4.hpp"

int main(int argc, char const *argv[]) {

  // parameters
  double r = 0.5;
  double m = 1.5;
  double gamma = 1.;
  double runtime = 10;
  int plotEvery = 2000;

  // create model and stepper
  NE *model = new NE(r, m, gamma, std::vector<int>{32, 32});
  Stepper *stepper = new ETDRK4();
  stepper->setModel(model);
  stepper->setTimestep(0.002 / r);

  // clear output folder
  output::clearOut();

  // set initial values
  model->setField(math::randoms(model->realPoints, 1e-3));

  // integrate
  auto timer = Timer("Integrating...", 5);
  while (timer.t < runtime) {
    // make time steps
    stepper->steps(plotEvery);
    model->resetZeroMode();
    // show progress and performance
    timer.show(timer.t / runtime);
    std::cout << "Performance:   "
              << std::round(model->t / stepper->dt / timer.t) << " steps / s"
              << std::endl;
  }

  delete model;
  delete stepper;
  return 0;
}
