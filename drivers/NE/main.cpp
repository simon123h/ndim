#include "generic.hpp"
#include "models/NE_ps.hpp"
#include "stepper/etdrk4.hpp"

int main(int argc, char const *argv[]) {

  // parameters
  double r = 0.5;
  double m = 4;
  double gamma = 1;
  double T_transient = 100 / r;
  double T_integrate = 20 / r;
  double T_lyapunov = 0 / r;
  int plotEvery = 20;

  // create model and stepper
  NE *model = new NE(r, m, gamma);
  Stepper *stepper = new ETDRK4();
  stepper->setModel(model);
  stepper->setTimestep(0.002 / r);
  auto *lyap = new measure::LyapunovExponents(stepper, 1);

  // set initial values
  model->setField(math::randoms(model->realPoints, 1e-3));
  // model->setField(output::load("out/field.dat"));

  // clear output folder
  output::clearOut();
  output::mkdir("out/dat");

  // save model and parameters
  model->save("out/model.ini");

  // integrate transient
  Timer timer = Timer("Integrating transient:", 4);
  while (model->t < T_transient) {
    timer.show(model->t / T_transient);
    stepper->steps(plotEvery);
    model->resetZeroMode();
  }

  /*
   * Time integration
   */
  model->t = 0;
  timer = Timer("Integrating dynamics:", 5);
  while (model->t < T_integrate) {
    // make time steps
    stepper->steps(plotEvery);
    model->resetZeroMode();
    // print progress
    int id = timer.show(model->t / T_integrate);
    std::cout << "Step " << id << std::endl;
    // save field
    output::save(output::format("out/dat/%05dr.dat", id), model->mesh->x,
                 model->getField());
    output::saveFourier(output::format("out/dat/%05df.dat", id), model->k,
                        model->getFourier(), model->mesh->points);
    if (model->rank == 1)
      output::save("out/spacetime.dat", model->getField(), true);
  }

  /*
   * Measure quantities
   */
  std::string fname = "out/quantities.dat";
  output::save(fname, "L2-norm: ");
  output::save(fname, measure::l2norm(model));
  output::save(fname, "Roughness: ");
  output::save(fname, measure::roughness(model));
  output::save(fname, "Drift: ");
  output::save(fname, measure::drift(model), true);
  output::save("out/field.dat", model->getField());
  system("cat out/quantities.dat");

  /*
   * Plot
   */
  if (model->rank == 1) {
    system("gnuplot spacetime.plt");
    system("gnuplot plot1d.plt > /dev/null 2>&1 &");
  }
  if (model->rank == 2) {
    system("gnuplot plot2d.plt > /dev/null 2>&1 &");
  }

  /*
   * Calculate lyapunov exponents
   */
  model->t = 0;
  timer = Timer("Calculating Lyapunov exponents:", 5);
  while (model->t < T_lyapunov) {
    lyap->step();
    timer.show(model->t / T_lyapunov);
    std::cout << "First LE: " << lyap->exponents[0] << std::endl;
    output::truncate("out/exponents.dat");
    output::save("out/exponents.dat", lyap->exponents);
  }

  delete model;
  delete stepper;
  delete lyap;
  return 0;
}
