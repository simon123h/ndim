
#include <Eigen/Dense>
#include <iostream>
using namespace std;
using namespace Eigen;
int main() {
  MatrixXd A(3, 3);
  VectorXd b(3);
  A << 1, 2, 3, 4, 5, 6, 7, 8, 10;
  b << 3, 3, 4;
  cout << "Here is the matrix A:\n" << A << endl;
  cout << "Here is the vector b:\n" << b << endl;
  Vector3d x = A.colPivHouseholderQr().solve(b);
  cout << "The solution is:\n" << x << endl;
}