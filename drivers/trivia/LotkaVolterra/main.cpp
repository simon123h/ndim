#include "models/LotkaVolterra.hpp"
#include "stepper/euler.hpp"
#include "stepper/rk4.hpp"
#include <iostream>

int main(int argc, char const *argv[]) {

  // create problem
  LotkaVolterra *model = new LotkaVolterra();
  Stepper *stepper = new RK4();
  // Stepper *stepper = new Euler();
  stepper->model = model;
  stepper->dt = 0.1;

  // set initial values
  model->vars = std::vector<double>{10, 10};
  model->a = 0.1;
  model->b = 0.02;
  model->c = 0.4;
  model->d = 0.02;

  const int STEPS = 10;

  for (int i = 0; i < STEPS; i++) {
    std::cout << i << " " << model->vars[0] << " " << model->vars[1]
              << std::endl;
    stepper->step();
  }

  delete model;
  delete stepper;

  return 0;
}
