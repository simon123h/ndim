#include "models/CoupledLogisticMap.hpp"
#include "models/LogisticMap.hpp"
#include "output/output.hpp"
#include "stepper/dsIterator.hpp"
#include <iostream>

int main(int argc, char const *argv[]) {

  // create problem
  CoupledLogisticMap *model = new CoupledLogisticMap();
  DSIterator *stepper = new DSIterator();
  stepper->model = model;

  // set initial values
  model->vars[0] = 0.5;
  model->r = 3.2;

  system("rm -rf out; mkdir -p out");
  std::cout << *model->parameters[0] << std::endl;
  std::cout << *model->parameters[1] << std::endl;
  model->save("out/model.ini");

  const int STEPS = 30;

  for (int i = 0; i < STEPS; i++) {
    std::cout << i << " " << model->vars[0] << " " << model->vars[1]
              << std::endl;
    stepper->step();
  }

  delete model;
  delete stepper;

  return 0;
}
