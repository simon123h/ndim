#!gnuplot

set terminal pngcairo size 960,480 enhanced font 'Verdana,10'

# flat view on heatmap
set view map

# plot aspect is original aspect ratio
set size ratio -1

# ranges dont stick to even numbers
set autoscale xfix
set autoscale yfix
set autoscale cbfix

# load parameters
# load "out/info/parameters.txt"

# load palette
load "../../tools/palettes/moreland.pal"

! mkdir -p out/img

# plot a single file or all?
if(exists("i")){
	start = i
	stop = i
	plotAll = 0
}else{
	start = 1
	stop = int(system("ls out/dat | tail -n1"))
	plotAll = 1
}

set xrange[*:*]
set yrange[*:*]
# maximum cb amplitude
amp = 2.5

# loop over images
do for [i=start:stop]{
	# format plotID
	id = sprintf('%05d', i)
	# output file name
	set out 'out/img/'.id.'.png'

	## real space plot
	set cbrange[*:*]
	stats 'out/dat/'.id.'r.dat' using 3 prefix "v" nooutput
	# if(v_max > amp){amp = v_max}
	# if(-v_min > amp){amp = -v_min}
	# set cbrange[-amp:amp]
	unset logscale
	set xtics pi
	set ytics pi
    set format x '%.0P π'
    set format y '%.0P π'
	set format cb '%g'
	# set multiplot layout 1,2
	# set multiplot layout 1,2 title '{/=14 '.rtitle.sprintf('t = %.3f}', t)

	set title "{/=14 v(x, y, t)}"
	plot 'out/dat/'.id.'r.dat' using 1:2:3 with image notitle

	if(plotAll == 1){
		print("Plot ".id)
	}
}
