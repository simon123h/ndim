#!gnuplot

set terminal pngcairo size 960,480 enhanced font 'Verdana,10'

# ranges dont stick to even numbers
# set autoscale xfix
# set autoscale yfix

! mkdir -p out/img

# plot a single file or all?
if(exists("i")){
	start = i
	stop = i
	plotAll = 0
}else{
	start = int("0".system("ls out/img | tail -n1"))
	stop = int(system("ls out/dat | tail -n1"))
	plotAll = 1
}

set xrange[*:*]
set yrange[*:*]

# loop over images
do for [i=start:stop]{

	# format plotID
	id = sprintf('%05d', i)

	# output file name
	set out 'out/img/'.id.'.png'


    set multiplot layout 1,2
    set xlabel "x"
    set ylabel "h"
	plot 'out/dat/'.id.'r.dat' using 1:2 with lines notitle
    set xlabel "r"
    set ylabel "L2-norm"
    plot 'out/trace.dat' using 1:2 with lines notitle dt '.', \
         'out/trace.dat' every ::i::i using 1:2 notitle, \
         'out/trace.dat' using 1:($4 == 0 ? 1/0 : $2) with lines notitle lc 1 lw 2
		#  'out/bifurcations.dat' using 1:2 title 'possible bifurcations'
    unset multiplot

	if(plotAll == 1){
		print("Plot ".id)
	}
}
