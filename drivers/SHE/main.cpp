#include "generic.hpp"
#include "math/math.hpp"
#include "models/SHE_ps.hpp"
#include "solver/keller.hpp"
#include "solver/natural.hpp"
#include "stepper/etdrk4.hpp"
#include <string>

int main(int argc, char const *argv[]) {

  // parameters
  double L = 240;
  int N = std::pow(2, 9);
  double r = -0.013;
  double kc = 0.5;
  double v = 0.41;
  double g = 1;
  double T_transient = 0;
  double T_integrate = 2000;
  double dt = 0.05;
  int plotEvery = 2000;

  // create model and stepper
  SHE *model = new SHE(L, N, r, kc, v, g);
  Stepper *stepper = new ETDRK4();
  stepper->setModel(model);
  stepper->setTimestep(dt);

  // set initial values
  std::vector<double> initial(model->realPoints);
  for (int i = 0; i < model->realPoints; i++) {
    double s = model->mesh->x[0][i] - L / 2.;
    double w = 10;
    double h0 = 1;
    double Pi = 3.141592;
    initial[i] = h0 * cos(2 * Pi * s / w) * exp(-0.005 * s * s);
  }
  model->setField(initial);

  // clear output folder
  output::clearOut();
  output::mkdir("out/dat");

  // save model and parameters
  model->save("out/model.ini");

  // integrate transient
  Timer timer = Timer("Integrating transient:", 4);
  while (model->t < T_transient) {
    timer.show(model->t / T_transient);
    stepper->steps(plotEvery);
  }

  /*
   * Time integration
   */
  model->t = 0;
  timer = Timer("Integrating dynamics:", 5);
  while (model->t < T_integrate) {
    // print progress
    int id = timer.show(model->t / T_integrate);
    std::cout << "Step " << id << std::endl;
    // save L2-norm
    output::save("out/trace.dat", model->r, measure::l2norm(model));
    // save field
    output::save(output::format("out/dat/%05dr.dat", id), model->mesh->x,
                 model->getField());
    if (model->rank == 1)
      output::save("out/spacetime.dat", model->getField(), true);
    // make time steps
    stepper->steps(plotEvery);
  }

  /*
   * Measure quantities
   */
  std::string fname = "out/quantities.dat";
  output::save(fname, "L2-norm: ");
  output::save(fname, measure::l2norm(model));
  output::save(fname, "Roughness: ");
  output::save(fname, measure::roughness(model));
  output::save(fname, "Drift: ");
  output::save(fname, measure::drift(model), true);
  output::save("out/field.dat", model->getField());
  system("cat out/quantities.dat");

  /*
   * Plot
   */
  // if (model->rank == 1) {
  //   system("gnuplot spacetime.plt");
  //   system("gnuplot plot1d.plt > /dev/null 2>&1 &");
  // }
  // if (model->rank == 2) {
  //   system("gnuplot plot2d.plt > /dev/null 2>&1 &");
  // }

  /*
   * Continuation
   */
  // auto *solver = new NaturalContinuation();
  auto *solver = new PseudoarclengthContinuation();
  solver->setModel(model);
  solver->parameter = &model->r;
  solver->stepSize = 1e-3;
  solver->epsilon = 1e-5;
  solver->adaptiveStepsize = true;
  solver->ds_decrease_factor = 0.2;
  solver->ds_increase_factor = 1.02;
  solver->constraint_scale = 0.1;
  bool converged = true;
  timer = Timer("Parameter continuation:", 0);
  int id = 0;
  while (converged) {
    // make a continuation step
    converged = solver->solve();
    solver->detectBifurcations();
    solver->checkStability();
    // print progress
    id = timer.show(0.1);
    // save L2-norm
    std::stringstream msg;
    msg << model->r << " " << measure::l2norm(model) << " ";
    msg << solver->point_type << " " << solver->is_point_stable;
    msg << std::endl;
    output::save("out/trace.dat", msg.str());
    // output::save("out/trace.dat", model->r, measure::l2norm(model));
    // save stable branches
    if (solver->is_point_stable)
      output::save("out/stable_trace.dat", model->r, measure::l2norm(model));
    // save bifurcation points
    if (solver->point_type == "BP")
      output::save("out/bifurcations.dat", model->r, measure::l2norm(model));
    // save field
    output::save(output::format("out/dat/%05dr.dat", id), model->mesh->x,
                 model->getField());
    std::cout << "ds: " << solver->stepSize << std::endl;
    // save eigenvalues
    if (solver->eigenvalues.size() > 0)
      output::save(output::format("out/dat/%05dev.dat", id),
                   math::real(solver->eigenvalues),
                   math::imag(solver->eigenvalues));
  }

  delete model;
  delete stepper;
  delete solver;
  return 0;
}
