#!gnuplot

set terminal pngcairo size 960,480 enhanced font 'Verdana,10'

# ranges dont stick to even numbers
# set autoscale xfix
# set autoscale yfix

! mkdir -p out/img

# plot a single file or all?
if(exists("i")){
	start = i
	stop = i
	plotAll = 0
}else{
	start = 0
	stop = int(system("ls out/dat | tail -n1"))
	plotAll = 1
}

set xrange[*:*]
set yrange[*:*]
set xlabel "x [a.u.]"
set xlabel "h"

# set logscale y

# loop over images
do for [i=start:stop]{

	# format plotID
	id = sprintf('%05d', i)

	# output file name
	set out 'out/img/'.id.'.png'

    # plot
	plot 'out/dat/'.id.'r.dat' using 1:2 with lines notitle

	if(plotAll == 1){
		print("Plot ".id)
	}
}
