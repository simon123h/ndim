#!/usr/bin/gnuplot
# set terminal pdf
# set out "out/bif.pdf"
set terminal pngcairo lw 1.5
set out "out/bif.png"
plot "out/stepper.dat" u 1:2 ps 0.2 notitle, \
     "out/solver.dat" u 1:2 wi li lw 2 dt "-" notitle, \
     "out/solver.dat" u 1:($2/$3) wi li lt 2 lw 2 notitle, \
     'out/bifurcations.dat' using 1:2 title 'possible bifurcations'