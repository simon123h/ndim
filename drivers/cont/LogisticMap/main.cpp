#include "generic.hpp"
#include "models/CoupledLogisticMap.hpp"
#include "models/LogisticMap.hpp"
#include "solver/keller.hpp"
#include "solver/newton.hpp"
#include "stepper/dsIterator.hpp"

int main(int argc, char const *argv[]) {

  // create problem
  LogisticMap *model = new LogisticMap();
  // CoupledLogisticMap *model = new CoupledLogisticMap();
  Stepper *stepper = new DSIterator();
  // Solver *solver = new Newton();
  auto *solver = new PseudoarclengthContinuation();
  stepper->setModel(model);
  solver->setModel(model);

  system("rm -rf out; mkdir -p out");
  model->save("out/model.ini");
  model->r = 0;

  // generate bifurcation diagram via time-stepping
  Timer timer = Timer("Time stepping", 4);
  while (model->r < 4) {
    model->r += 0.001;
    model->vars[0] = math::random(-1, 1);
    stepper->steps(1000);
    output::save("out/stepper.dat", model->r, model->vars[0]);
    timer.show(model->r / 4);
  }

  // generate initial solution for continuation
  model->vars[0] = 0.5;
  model->r = 3.5;
  stepper->steps(1000);
  // parameter continuation
  solver->parameter = &model->r;
  solver->stepSize = 0.05;
  solver->adaptiveStepsize = true;
  while (model->r > 0) {
    model->r -= 0.01;
    solver->solve();
    solver->detectBifurcations();
    solver->checkStability();
    double stab = solver->is_point_stable ? 1 : 0;
    output::save("out/solver.dat", model->r, model->vars[0], stab);
    // save bifurcation points
    if (solver->point_type == "BP")
      output::save("out/bifurcations.dat", model->r, model->vars[0]);
  }

  system("gnuplot plot.plt");

  delete model;
  delete stepper;
  delete solver;
  return 0;
}
