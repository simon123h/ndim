#include "generic.hpp"
#include "models/NE_ps.hpp"
#include "solver/keller.hpp"
#include "solver/natural.hpp"
#include "stepper/etdrk4.hpp"

int main(int argc, char const *argv[]) {

  // parameters
  double r = 0.5;
  double m = 1.05;
  double gamma = 1;
  double T_transient = 100 / r;
  double T_integrate = 10 / r;
  int plotEvery = 20;

  // create model
  NE *model = new NE(r, m, gamma, std::vector<int>{16, 16});
  // restrict some modes to break solution invariances
  // ignore zero and imag part of first mode, reducing the dimension
  model->ignoreModesReal.push_back(0);
  if (model->rank == 1) {
    model->ignoreModesImag.push_back(
        model->fft->pos2index(std::vector<int>{1}));
    model->setDimension(model->dimension - 2);
  } else {
    model->ignoreModesImag.push_back(
        model->fft->pos2index(std::vector<int>{0, 1}));
    model->ignoreModesImag.push_back(
        model->fft->pos2index(std::vector<int>{1, 0}));
    model->setDimension(model->dimension - 3);
  }

  // create stepper
  Stepper *stepper = new ETDRK4();
  stepper->setModel(model);
  stepper->setTimestep(0.002 / r);

  // clear output folder
  output::clearOut();
  output::mkdir("out/dat");

  // set initial values
  model->setField(math::randoms(model->realPoints, 1e-3));

  // integrate transient
  Timer timer = Timer("Integrating transient:", 4);
  while (model->t < T_transient) {
    timer.show(model->t / T_transient);
    stepper->steps(plotEvery);
  }

  /*
   * Time integration
   */
  model->t = 0;
  timer = Timer("Integrating dynamics:", 5);
  while (model->t < T_integrate) {
    // make time steps
    stepper->steps(plotEvery);
    // print progress
    int id = timer.show(model->t / T_integrate);
    std::cout << "Step " << id << std::endl;
    // save field
    output::save(output::format("out/dat/%05dr.dat", id), model->mesh->x,
                 model->getField());
    output::saveFourier(output::format("out/dat/%05df.dat", id), model->k,
                        model->getFourier(), model->mesh->points);
    if (model->rank == 1)
      output::save("out/spacetime.dat", model->getField(), true);
  }

  /*
   * Measure quantities
   */
  std::string fname = "out/quantities.dat";
  output::save(fname, "L2-norm: ");
  output::save(fname, measure::l2norm(model));
  output::save(fname, "Roughness: ");
  output::save(fname, measure::roughness(model));
  output::save(fname, "Drift: ");
  output::save(fname, measure::drift(model), true);
  output::save("field.dat", model->getField());
  system("cat out/quantities.dat");

  /*
   * Plot
   */
  // if (model->rank == 1) {
  //   system("gnuplot spacetime.plt");
  //   system("gnuplot plot1d.plt > /dev/null 2>&1 &");
  // }
  // if (model->rank == 2) {
  //   system("gnuplot plot2d.plt > /dev/null 2>&1 &");
  // }

  /*
   * Continuation
   */
  double m_max = 1.999;
  double m_min = 1.001;
  double m_start = model->m;
  auto *solver = new PseudoarclengthContinuation();
  solver->setModel(model);
  solver->parameter = &model->m;
  solver->stepSize = 0.05;
  solver->adaptiveStepsize = true;
  bool converged = true;
  timer = Timer("Parameter continuation:", 4);
  while (model->m < m_max && model->m > m_min && converged) {
    // make a continuation step
    converged = solver->solve();
    solver->detectBifurcations();
    solver->checkStability();
    // print progress
    int id = timer.show((model->m - m_start) / (m_max - m_start));
    // save roughness
    output::save("out/trace.dat", model->m, measure::roughness(model));
    // save stable branches
    if (solver->is_point_stable)
      output::save("out/stable_trace.dat", model->m, measure::roughness(model));
    // save bifurcation points
    if (solver->point_type == "BP")
      output::save("out/bifurcations.dat", model->m, measure::roughness(model));
    // save field
    output::save(output::format("out/dat/%05dcr.dat", id), model->mesh->x,
                 model->getField());
    output::saveFourier(output::format("out/dat/%05dcf.dat", id), model->k,
                        model->getFourier(), model->mesh->points);
    // save eigenvalues
    if (solver->eigenvalues.size() > 0)
      output::save(output::format("out/dat/%05dev.dat", id),
                   math::real(solver->eigenvalues),
                   math::imag(solver->eigenvalues));
  }

  delete model;
  delete stepper;
  return 0;
}
