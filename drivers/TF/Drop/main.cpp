#include "generic.hpp"
#include "models/Drop_ps.hpp"
// #include "models/TF_Drop.hpp"
#include "stepper/etdrk4.hpp"
#include "stepper/euler.hpp"
#include "stepper/rk4.hpp"

int main(int argc, char const *argv[]) {

  // parameters
  double alpha = 3;
  double L = 1000;
  double T_transient = 0;
  double T_integrate = 1000;
  int plotEvery = 10000;

  // create model and stepper
  auto *model = new Drop(L, 256);
  // Stepper *stepper = new Euler();
  Stepper *stepper = new ETDRK4();
  stepper->setModel(model);
  stepper->setTimestep(0.0001);

  // set initial values
  std::vector<double> init(model->mesh->npoints);
  for (int i = 0; i < model->mesh->npoints; i++) {
    double w = L / 2.;
    double h1 = w / 50.;
    double s = L * i / (double)model->mesh->npoints - L / 2.;
    init[i] = 1;
    if (std::abs(s) < w / 2)
      init[i] += h1 * cos(3.141592 * s / w);
  }
  model->setField(init);

  // clear output folder
  output::clearOut();
  output::mkdir("out/dat");

  // save model and parameters
  model->save("out/model.ini");

  // integrate transient
  Timer timer = Timer("Integrating transient:", 4);
  while (model->t < T_transient) {
    timer.show(model->t / T_transient);
    stepper->steps(plotEvery);
  }

  /*
   * Time integration
   */
  model->t = 0;
  timer = Timer("Integrating dynamics:", 5);
  while (model->t < T_integrate) {
    // make time steps
    // print progress
    int id = timer.show(model->t / T_integrate);
    std::cout << "Step " << id << std::endl;
    // save field
    output::save(output::format("out/dat/%05d.dat", id), model->mesh->x,
                 model->getField());
    output::save("out/spacetime.dat", model->getField(), true);
    stepper->steps(plotEvery);
    // model->U += 0.001;
  }

  /*
   * Measure quantities
   */
  std::string fname = "out/quantities.dat";
  output::save(fname, "L2-norm: ");
  output::save(fname, measure::l2norm(model));
  output::save(fname, "Roughness: ");
  output::save(fname, measure::roughness(model));
  output::save(fname, "Drift: ");
  output::save(fname, measure::drift(model), true);
  output::save("out/field.dat", model->getField());
  system("cat out/quantities.dat");

  /*
   * Plot
   */
  system("gnuplot plot1d.plt > /dev/null 2>&1 &");
  system("gnuplot spacetime.plt");

  delete model;
  delete stepper;
  return 0;
}
