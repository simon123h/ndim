#!gnuplot

set terminal pngcairo lw 2 size 960,480 enhanced font 'Verdana,10'

# ranges dont stick to even numbers
# set autoscale xfix
# set autoscale yfix

! mkdir -p out/img

# plot a single file or all?
if(exists("i")){
	start = i
	stop = i
	plotAll = 0
}else{
	start = 0
	stop = int(system("ls out/dat | tail -n1"))
	plotAll = 1
}

set xrange[0:*]
set yrange[1:120]
set yrange[*:*]
set xlabel "x/L"
set ylabel "h(x)"
# set logscale y

# set logscale y

# loop over images
do for [i=start:stop]{

	# format plotID
	id = sprintf('%05d', i)

	# output file name
	set out 'out/img/'.id.'.png'

    # plot
	plot 'out/dat/'.id.'.dat' using ($1/1000):2 with lines notitle

	if(plotAll == 1){
		print("Plot ".id)
	}
}
