#!/usr/bin/gnuplot
set terminal pngcairo

# load palette
load "../../../tools/palettes/moreland.pal"

set autoscale xfix
set autoscale yfix
set out "out/spacetime.png"
plot "out/spacetime.dat" matrix with image notitle

